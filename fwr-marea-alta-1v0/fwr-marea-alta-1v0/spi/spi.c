/*
 * spi.c
 *
 * Created: 18-03-2020 11:32:50
 *  Author: hardw
 */ 

#include "spi.h"

// Init SPI0 in Master Mode
void SPI0_MasterInit(void)
{
	
	DDRB |= ((1<<PORTB5) | (1<<PORTB7));							//	Set MOSI and SCK output, all others input
	SPCR0 |= ((1<<SPE) | (1<<MSTR) | (1<<SPR0) | (1<<CPHA));		//	Enable SPI, Master, set clock rate fclk/16
}

// Perform SPI Master transaction 
unsigned char SPI0_transceiver(unsigned char cData)
{
	SPDR0 = cData;
	while(!(SPSR0 & (1<<SPIF)));
	return SPDR0;
}