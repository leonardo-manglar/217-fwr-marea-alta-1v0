/*
 * spi.h
 *
 * Created: 18-03-2020 11:33:28
 *  Author: hardw
 */ 


#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>

// Init SPI0 in Master Mode
void SPI0_MasterInit(void);

// Perform SPI Master transaction 
unsigned char SPI0_transceiver(unsigned char cData);


#endif /* SPI_H_ */