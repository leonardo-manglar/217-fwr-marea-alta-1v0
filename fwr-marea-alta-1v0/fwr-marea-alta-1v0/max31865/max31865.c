/*
 * max31865.c
 *
 * Created: 18-03-2020 13:39:48
 *  Author: hardw
 */ 

#include "max31865.h"

//	Initializes a max31865 SPI peripheral in the MCU. Peripheral as slave, MCU as master 
void max31865_slave_init(unsigned char slave_select_pin)
{
	DDRB |= (1<<slave_select_pin);		//	Chip Select pin to output
	PORTB |= (1<<slave_select_pin);		//	Chip Select pin HIGH
}

// Starts ADC conversion and returns temperature value (raw value, not Celsius)
unsigned int max31865_get_RTD_val(unsigned char slave_select_pin, unsigned char drdy_pin)
{
	unsigned int temp;
	
	PORTB &= ~(1<<slave_select_pin);	//	SS Low
	SPI0_transceiver(0x80);				//	Configuration Register Address
	SPI0_transceiver(0xA1);				//	Single Shot conversion
	PORTB |= (1<<slave_select_pin);		//	SS High
	
	while(PORTB & (1<<drdy_pin));			//	Wait for conversion ready
	
	PORTB &= ~(1<<slave_select_pin);	//	SS Low
	SPI0_transceiver(0x01);				//	Request RTD MSB
	temp = (unsigned int)(SPI0_transceiver(0x02)<<7);		//	Store RTD MSB and request RTD LSB
	temp |= (unsigned int)(SPI0_transceiver(0x00)>>1);		//	Store RTD LSB, sending any address just to fetch new shift register value from SPI
	PORTB |= (1<<slave_select_pin);		//	SS High
	
	return temp;
}