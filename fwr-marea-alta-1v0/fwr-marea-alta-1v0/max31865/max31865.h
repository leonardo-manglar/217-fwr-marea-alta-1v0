/*
 * max31865.h
 *
 * Created: 18-03-2020 13:40:06
 *  Author: hardw
 */ 

#ifndef MAX31865_H_
#define MAX31865_H_

#include <avr/io.h>

//	Initializes a max31865 SPI peripheral in the MCU. Peripheral as slave, MCU as master 
void max31865_init(unsigned char slave_select_pin);

// Starts ADC conversion and returns temperature value (raw value, not Celsius)
unsigned int max31865_get_RTD_val(unsigned char slave_select_pin, unsigned char drdy_pin);

#endif /* MAX31865_H_ */