/*
 * lora_util_msgs.c
 *
 * Created: 12-Jul-18 5:30:44 PM
 *  Author: Diego Hinojosa
 */ 
#include "lora_util_msgs.h"

//11 bytes messages

void lora_msg_string_to_hexstring(const char * string, char * hexstring )
{	
	{
		unsigned char ch,i,j,len;
		
		len = strlen(string);
		
		for(i=0,j=0;i<len;i++,j+=2)
		{
			ch = string[i];
			
			if( ch >= 0 && ch <= 0x0F)
			{
				hexstring[j] = 0x30;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x10 && ch <= 0x1F)
			{
				hexstring[j] = 0x31;
				ch -= 0x10;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x20 && ch <= 0x2F)
			{
				hexstring[j] = 0x32;
				ch -= 0x20;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x30 && ch <= 0x3F)
			{
				hexstring[j] = 0x33;
				ch -= 0x30;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x40 && ch <= 0x4F)
			{
				hexstring[j] = 0x34;
				ch -= 0x40;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x50 && ch <= 0x5F)
			{
				hexstring[j] = 0x35;
				ch -= 0x50;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x60 && ch <= 0x6F)
			{
				hexstring[j] = 0x36;
				ch -= 0x60;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
			else  if( ch >= 0x70 && ch <= 0x7F)
			{
				hexstring[j] = 0x37;
				ch -= 0x70;
				
				if(ch >= 0 && ch <= 9)
				hexstring[j+1] = 0x30 + ch;
				else
				hexstring[j+1] = 0x37 + ch;
			}
		}
		hexstring[j] = 0x00;
	}
}

void lora_msg_int_to_hexstring(const uint32_t data, char * hexstring, uint8_t len_bytes)
{
	//num_min not used!
	//max number 0f ff ff = 1048575
	//dbg_prints("-------------");

	char msg_format[10];
	uint8_t num_bytes = 0;
	uint8_t len1=0,len2=0;

	if (len_bytes==0)
	{
		if((data>>8)==0){
			num_bytes = 1;
		}else if((data>>16)==0){
			num_bytes = 2;
		}else if((data>>24)==0){
			num_bytes = 3;
		}else if((data>>32)==0){
			num_bytes = 4;
		}
	}else{
		num_bytes=len_bytes;		
	}
//	//DEBUG: to print formating
//  dbg_printSn("Bytes:",num_bytes);
// 	char buff1[5];
// 	char buff2[5];
// 	sprintf(buff1,"%x",(unsigned int)data);
// 	sprintf(buff2,"%x",(unsigned int)(data>>16));
// 	dbg_printSS("unf_data1:",buff1);
// 	dbg_printSS("unf_data2:",buff2);
	
	if (num_bytes<=1){
		sprintf(msg_format,"%02X",data);
		
	}else if(num_bytes<=2){
		sprintf(msg_format,"%04X",data);
		
	}
	else if(num_bytes<=3){
		uint16_t temp=(data>>16);
		sprintf(msg_format,"%02X%04X",temp,data);
		
	}else if(num_bytes<=4){
		uint16_t temp=(data>>16);
		sprintf(msg_format,"%04X%04X",temp,data);
	}
 	
	//DEBUG: to print data format
 	//dbg_printSS("data:",msg_format);
	 
 	strcpy(hexstring,msg_format);//mandatory
}

