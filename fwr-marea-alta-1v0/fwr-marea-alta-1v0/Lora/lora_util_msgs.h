/*
 * lora_util_msgs.h
 *
 * Created: 12-Jul-18 5:30:33 PM
 *  Author: Diego Hinojosa
 */ 


#ifndef LORA_UTIL_MSGS_H_
#define LORA_UTIL_MSGS_H_

#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include "serial_debug.h"

void lora_msg_string_to_hexstring(const char * msg_string, char * msg_hex );
void lora_msg_int_to_hexstring(const uint32_t data, char * hexstring, uint8_t num_min);




#endif /* LORA_UTIL_MSGS_H_ */