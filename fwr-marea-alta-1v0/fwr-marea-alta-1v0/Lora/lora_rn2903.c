/*
 * lora_rn2903.c
 *
 * Created: 27-Jun-18 12:35:27 PM
 *  Author: Diego Hinojosa
 */ 

#include "lora_rn2903.h"
/* new functions */
static void rn2903_send_command(const char * command);
static enum rn2903_res_status rn2903_get_response(char * response, uint8_t len_response);
static enum rn2903_res_status rn2903_get_response_t40(char * response, uint8_t len_response);

//To send a command
static void rn2903_send_command(const char * command)
{
	uart0_putstring(command);
}

//Wait an answer 15 sec
static enum rn2903_res_status rn2903_get_response(char * response, uint8_t len_response)
{
	uart0_getResponse_t(response,len_response);
	return RN_OK;
}

//Wait an answer 40 sec
static enum rn2903_res_status rn2903_get_response_t40(char * response, uint8_t len_response)
{
	uart0_getResponse_t40(response,len_response);
	return RN_OK;
}

//To send a command and wait for an answer
enum rn2903_res_status rn2903_send_simple_command(const char * command, const char * resp_expect){
	char response[40];
	memset(response, 0, sizeof(response));
	
	rn2903_send_command(command);
	rn2903_get_response(response, sizeof(response));
	
	//dbg_prints(response);//*dbg
	if (checkResponse(response,resp_expect) == RN_OK){
		//printDebug("Expected response\n");
		return RN_OK;
	}
	return RN_ERR;
}

//To set parameters
enum rn2903_res_status rn2903_send_simple_command_param(const char *  command, const char *  param1){
	char full_command[80] ;
	memset(full_command,0,80);
	
	int result = RN_ERR;
	
//	sprintf(full_command,"%s %s%s",command,param1,END_COMMAND);
	strcat(full_command,command);
	strcat(full_command,param1);
	strcat(full_command,END_COMMAND);
	
	//dbg_prints(full_command);
	
	result = rn2903_send_simple_command(full_command, RESP_REQUEST_SUCCESS);				// set nwkskey
	return result;
}


enum rn2903_res_status rn2903_reset_factory(void){
	// Expected response: RN2903 X.Y.Z MMM DD YYYY HH:MM:SS
	char response[70];
	rn2903_send_command(COM_FACTORY_RESET);	// reset factory command
	rn2903_get_response(response, sizeof(response));				// wait response
	dbg_prints(response);//debug
	if (inString("RN2903",response)==0){
		return RN_OK;
	}else{
		return RN_ERR;
	}
}

enum rn2903_res_status rn2903_set_deveui(const char * deveui_str)
{
	char buff[34];
	//uint8_t deb_len = strlen(deveui_str); //16
	//dbg_printn(deb_len);
	
	sprintf(buff,"%s %s%s", COM_SET_DEVEUI, deveui_str,END_COMMAND);
	//dbg_prints(buff);
	//dbg_printn(strlen(buff));
	
	if (rn2903_send_simple_command(buff, RESP_REQUEST_SUCCESS)!=RN_OK){
		return RN_ERR;
	}
	return RN_OK;
}

enum rn2903_res_status rn2903_set_appkey(const char * appkey_str)
{
	char buff[50];
	//uint8_t deb_len = strlen(appkey_str); //32
	//dbg_printn(deb_len);
	
	sprintf(buff,"%s %s%s", COM_SET_APPKEY, appkey_str,END_COMMAND);
	//dbg_prints(buff);
	//dbg_printn(strlen(buff));
	if (rn2903_send_simple_command(buff, RESP_REQUEST_SUCCESS)!=RN_OK){
		return RN_ERR;
	}
	return RN_OK;
}

enum rn2903_res_status rn2903_set_appeui(const char * appeui_str)
{
	char buff[34];
	//uint8_t deb_len = strlen(appeui_str); //16
	//dbg_printn(deb_len);
	
	sprintf(buff,"%s %s%s", COM_SET_APPEUI, appeui_str,END_COMMAND);
	//dbg_prints(buff);
	//dbg_printn(strlen(buff));
	if (rn2903_send_simple_command(buff, RESP_REQUEST_SUCCESS)!=RN_OK){
		return RN_ERR;
	}
	return RN_OK;
}

enum rn2903_res_status rn2903_set_devaddr(const char * devaddr_str)
{
	char buff[26];
	//uint8_t deb_len = strlen(devaddr_str); //8
	//dbg_printn(deb_len);
	
	sprintf(buff,"%s %s%s", COM_SET_DEVADDR, devaddr_str,END_COMMAND);
	//dbg_prints(buff);
	//dbg_printn(strlen(buff));
	if (rn2903_send_simple_command(buff, RESP_REQUEST_SUCCESS)!=RN_OK){
		return RN_ERR;
	}
	return RN_OK;
}


/*
 *Function Lora_block_channels
 *Blocks all channels that are disabled in the gateway.
 *Channels: 0-71
 *Return: 0:Success, 1:Error
 */
enum rn2903_res_status rn2903_set_channels(void)
{
	char full_command[20] = "";	
	char response[10] = "";
	
	//dbg_prints("set channels");
	
	for(uint8_t channel=0;channel<72 ; channel++)
	{
		//dbg_printn(channel);
		if(((channel>=0)&&(channel<=8))||(channel==65)){
			sprintf(full_command, "%s %i %s\r\n",COM_CHANNEL_SET,channel,COM_ACTIVE_CHAN);	
		}else{

			sprintf(full_command, "%s %i %s\r\n",COM_CHANNEL_SET,channel,COM_DEACTIVE_CHAN);
		}
		rn2903_send_simple_command(full_command, RESP_REQUEST_SUCCESS);
	}
	
	//dbg_prints(full_command);
	rn2903_send_simple_command(COM_SAVE,RESP_REQUEST_SUCCESS);
	
	rn2903_get_response(response,10);
	dbg_prints(response);
	return RN_OK;
}


/*
 *Function getInnerData
 *Gets internal data from server
 *First 4 character in base64 message indicate the code, other is data 
 */
enum rn2903_res_status rn2903_get_data_downlink(char * allData, rn2903_module * lora_data){

	strcpy(lora_data->downlink,allData);
	
	return RN_OK;
}



enum rn2903_res_status  rn2903_init_module(const rn2903_module * module)
{	
	if(rn2903_set_deveui(module->deveui)!=RN_OK) return RN_ERR;
	if(rn2903_set_appeui(module->appeui)!=RN_OK) return RN_ERR;	
	if(rn2903_set_appkey(module->appkey)!=RN_OK) return RN_ERR;
	if(rn2903_set_adr(module->adr)!=RN_OK) return RN_ERR;
	if(rn2903_set_data_rate(module->dr)!=RN_OK) return RN_ERR;
	if(rn2903_mac_save()!=RN_OK) return RN_ERR;

	return RN_OK;
}

enum rn2903_res_status rn2903_set_parameters(rn2903_module * module, const char * deveui, const char * appeui, const char * appkey)
{
// 	dbg_printn(strlen(deveui));
// 	dbg_printn(strlen(appeui));
// 	dbg_printn(strlen(appkey));
	
	if(strlen(deveui)!=16){
		dbg_prints("deveui_len_error");
		return RN_ERR;
	}
	if(strlen(appeui)!=16){
		dbg_prints("appeui_len_error");
		return RN_ERR;
	}
	if(strlen(appkey)!=32){
		dbg_prints("appkey_len_error");
		return RN_ERR;
	}
	strcpy(module->deveui,deveui);
	strcpy(module->appeui,appeui);
	strcpy(module->appkey,appkey);
	return RN_OK;	
}


enum rn2903_res_status rn2903_send_command_double_res(const char * command, const char * resp_expected1,const char * resp_expected2){
	char response[20];
	memset(response,0,sizeof(response));
		
	rn2903_send_command(command);
	
	rn2903_get_response(response, sizeof(response));
	
	//dbg_prints(response);//dbg
	
	if (checkResponse(response,resp_expected1)!= RN_OK){
		return RN_ERR;
	}	
	
	memset(response,0,sizeof(response));
	
	rn2903_get_response(response, sizeof(response));
	
	dbg_prints(response);//dbg
	
	if (checkResponse(response,resp_expected2)==0){
		return RN_OK;
	}else{
		return RN_ERR;
	}
}



// /*
//  *Function Lora_sendCommand
//  *
//  *Params: 
//  *Return:
//  */
// enum rn2903_result rn2903_send_simple_command(const char * command, const char * resp_expected1,const char * resp_expected2){
// 	char response[20];
// 	memset(response,0,sizeof(response));
// 		
// 	rn2903_send_command(command);
// 	
// 	rn2903_get_response(response, sizeof(response));
// 	if (checkResponse(response,resp_expected1)!= RN_OK){
// 		return RN_ERR;
// 	}	
// 	
// 	memset(response,0,sizeof(response));
// 	
// 	rn2903_get_response(response, sizeof(response));
// 	if (checkResponse(response,resp_expected2)==0){
// 		return RN_OK;
// 	}else{
// 		return RN_ERR;
// 	}
// }

/*
 *Function rn2903_send_cnf_data
 *Sends data through lora CNF,this function will expect an acknowledgment back from the server.
 *fport to send can be any number in range 1-223 
 *Params: fport, data , *dest and pointer to the register to return data from server.
 *Return success if the message was received
 */
enum send_msg_responses rn2903_send_cnf_data(const char *fport, const char * data_hex_str, rn2903_module * lora_module)
{ 
	char response[20];
	char full_command[40];
	memset(full_command,0,sizeof full_command);	
	
	sprintf(full_command,"%s %s %s%s",COM_CNF_SEND,fport,data_hex_str,END_COMMAND);
	//dbg_printSn("Len:",strlen(data_hex_str));
	//dbg_prints(full_command);			//dbg
	
	rn2903_send_command(full_command);		// send full command	
	
	memset(response,0,20);						// clear the buffer to store the response 
	rn2903_get_response(response, sizeof(response));		// waits until the response arrives or timeout in 15 sec

	//dbg_prints(response);//dbg	
	
	if (checkResponse(response,RESP_REQUEST_SUCCESS)!=0){	
		
		if (checkResponse(response,RESP_INVALID_PARAMETER)==RN_OK){
			return ERROR_INVALID_PARAM;
			
		}else if (checkResponse(response,RESP_NOT_JOINED)==RN_OK){
			return ERROR_NOT_JOINED;
		
		}else if (checkResponse(response,RESP_NO_FREE_CHANNEL)==RN_OK){
			return ERROR_NO_FREE_CHAN;

		}else if (checkResponse(response,RESP_SILENT)==RN_OK){
			return ERROR_SILENT;
		
		}else if (checkResponse(response,RESP_FRAME_COUNTER)==RN_OK){
			return ERROR_FRAME_COUNT_ERR;
		
		}else if (checkResponse(response,RESP_BUSY)==RN_OK){
			return ERROR_BUSY;
		
		}else if (checkResponse(response,RESP_MAC_PAUSED)==RN_OK){
			return ERROR_MAC_PAUSED;
		
		}else if (checkResponse(response,RESP_ERR_DATA_LEN)==RN_OK){
			return ERROR_INVALID_DATA_LEN;	
		}
		return ERROR_UNKNOW;
	}
	
	memset(response,0,20);
	
	rn2903_get_response_t40(response, sizeof(response));   // get response from server, wait 40 sec
	//strcpy(response,ERROR_MAC_ERROR); //DEBUG: harcoded response tot test uplink error counter
	//dbg_prints(response);//dbg	
	
	
	if (inString(RESP_RECEIVE_DATA,response)==RN_OK){  // if RECEIVE_DATA string is inside response, get the data
		rn2903_get_data_downlink(response, lora_module); 
		return UPLNK_DATA_OK;
	}else if(checkResponse(response,RESP_SEND_DATA_MAC_SUCCES)==RN_OK){	// response is ok but does not have a message
		return UPLNK_OK;
	}else if(checkResponse(response,RESP_MAC_ERROR)==RN_OK){
		return ERROR_MAC_ERROR;
	}else if (checkResponse(response,RESP_BUSY)==RN_OK){
		return ERROR_BUSY;
	}else if (checkResponse(response,RESP_ERR_DATA_LEN)==RN_OK){
		return ERROR_INVALID_DATA_LEN;	
	}else{
		return ERROR_UNKNOW;
	}
}

/*
 *Function Lora_send_uncnf_data
 *Sends data through lora UNCNF, this function will not expect any acknowledgment back from the server.
 *Params: fport , data , response1, response2, and pointer to the register to return data from server.
 *Return success if the message was received
 */
//uint8_t Lora_send_uncnf_data(char *fport, char * data, char * dest){ 
enum send_msg_responses rn2903_send_uncnf_data(const char *fport, const char * data_hex_str, rn2903_module * lora_module)
{ 
	char response[20];
	char full_command[40];
	memset(full_command,0,sizeof full_command);
	
	sprintf(full_command,"%s %s %s%s",COM_UNCNF_SEND,fport,data_hex_str,END_COMMAND);
	dbg_printSn("Len:",strlen(data_hex_str));
	//dbg_prints(full_command);			//dbg
	
	rn2903_send_command(full_command);		// send full command
	
	memset(response,0,sizeof response);						// clear the buffer to store the response
	rn2903_get_response(response, sizeof(response));		// waits until the response arrives or timeout in 15 sec

	//dbg_prints(response);//dbg
	
	if (checkResponse(response,RESP_REQUEST_SUCCESS)!=0){
		
		if (checkResponse(response,RESP_INVALID_PARAMETER)==RN_OK){
			return ERROR_INVALID_PARAM;
			
			}else if (checkResponse(response,RESP_NOT_JOINED)==RN_OK){
			return ERROR_NOT_JOINED;
			
			}else if (checkResponse(response,RESP_NO_FREE_CHANNEL)==RN_OK){
			return ERROR_NO_FREE_CHAN;

			}else if (checkResponse(response,RESP_SILENT)==RN_OK){
			return ERROR_SILENT;
			
			}else if (checkResponse(response,RESP_FRAME_COUNTER)==RN_OK){
			return ERROR_FRAME_COUNT_ERR;
			
			}else if (checkResponse(response,RESP_BUSY)==RN_OK){
			return ERROR_BUSY;
			
			}else if (checkResponse(response,RESP_MAC_PAUSED)==RN_OK){
			return ERROR_MAC_PAUSED;
			
			}else if (checkResponse(response,RESP_ERR_DATA_LEN)==RN_OK){
			return ERROR_INVALID_DATA_LEN;
		}
		return ERROR_UNKNOW;
	}
	
	memset(response,0,sizeof(response));
	
	rn2903_get_response_t40(response, sizeof(response));   // get response from server, wait 40 sec
	
	//dbg_prints(response);//dbg
	
	if (inString(RESP_RECEIVE_DATA,response)==RN_OK){  // if RECEIVE_DATA string is inside response, get the data
		rn2903_get_data_downlink(response, lora_module);
		return UPLNK_DATA_OK;
		}else if(checkResponse(response,RESP_SEND_DATA_MAC_SUCCES)==RN_OK){	// response is ok but does not have a message
		return UPLNK_OK;
		}else if(checkResponse(response,RESP_MAC_ERROR)==RN_OK){
		return ERROR_MAC_ERROR;
		}else if (checkResponse(response,RESP_BUSY)==RN_OK){
		return ERROR_BUSY;
		}else if (checkResponse(response,RESP_ERR_DATA_LEN)==RN_OK){
		return ERROR_INVALID_DATA_LEN;
		}else{
		return ERROR_UNKNOW;
	}
}

/*
 *Function Lora_checkModule
 *Return success if the model of lora module is the RN2903
 */
enum rn2903_conn_status rn2903_check_module(void){
	char response[70];
	memset(response,0,70);
	
	rn2903_send_command(COM_GET_VERSION);				// send command
	rn2903_get_response(response, sizeof(response));	// wait response
	//dbg_prints(response); //debug: show response from rn2903

	uint8_t comm = inString("RN2903",response);
	uint8_t firmw = inString("1.0.5",response);
	if (comm==0 && firmw == 0)
	{
		dbg_prints("RN OK");
		return RN_CONN_OK;
	}else if(comm==0 && firmw == 1){
		dbg_prints("RN FW ERR");
		//TODO: test with rn2903 not updated!
		return RN_CONN_ERR;
	}else{
		//RN2483
		dbg_prints("RN COM ERR");
		return RN_CONN_ERR;
	}
}




uint8_t rn2903_reset(void)
{
	// Expected response: RN2903 X.Y.Z MMM DD YYYY HH:MM:SS
	char response[40];
	uart0_putstring(COM_RESET);	// reset factory command
	uart0_getResponse_t(response, sizeof(response));				// wait response
	if (inString("RN2903",response)==0){
		return RN_OK;
	}else{
		return RN_ERR;
	}
}


// void Lora_getDataServer(char * response,char * dest){
// 	char * token;
// 	token = strtok(response," ");
// 	//printDebug(token); //debug
// 	uint16_t index = 1;
// 	while(token != NULL){
// 		token = strtok(NULL," ");
// 		if(index==1){
// 			//command
// 			//mac_rx
// 		}
// 		else if(index==2){
// 			sprintf(dest, "%s",token);
// 			token = strtok(dest,"\r");
// 			sprintf(dest, "%s",token);
// 			//onRed();
// 		}
// 		index++;
// 	}	
// }




enum rn2903_join_res_status rn2903_join(void){
	if (rn2903_send_command_double_res(COM_JOIN_OTAA, RESP_REQUEST_SUCCESS, RESP_JOIN_SUCCESS)==RN_OK){
		return RN_JOIN_ACCEPTED;
	}
	return RN_JOIN_ERR;
}



enum rn2903_res_status rn2903_get_status(char * status)
{
	//dbg_prints("get status");
	char response[10]="";
	memset(response,0,10);
	rn2903_send_command(COM_GET_STATUS);
	rn2903_get_response(response, 10);
	//dbg_prints(response); //debug
	strcpy(status,response);
	return 0;
}

enum rn2903_join_status rn2903_is_join(void)
{	
	char response[10]="";
	char tmp_status[10]="";
	rn2903_get_status(response);
	for (uint8_t i=0;i<8;i++){
		tmp_status[i]=response[i];
	}
	uint8_t status = atoi(tmp_status);
	if ((status&0x01)==1){
		return RN_JOINED;
		dbg_printn(1);
	}else{
		return RN_NOT_JOINED;
		dbg_printn(2);
	}
}

enum rn2903_res_status rn2903_set_bat(const uint8_t bat_level)
{	
	char str_num[5];
	sprintf(str_num,"%d",bat_level);
	rn2903_send_simple_command_param("mac set bat ",str_num);
	return RN_OK;
}


/*	 DataRate
 * 		0: 		SF = 10, BW = 125 kHz, BitRate =   980 bps, max payload = 11  bytes
 *  	1: 		SF = 9,  BW = 125 kHz, BitRate =  1760 bps, max payload = 53  bytes
 *  	2: 		SF = 8,  BW = 125 kHz, BitRate =  3125 bps, max payload = 129 bytes
 *  	3: 		SF = 7,  BW = 125 kHz, BitRate =  5470 bps, max payload = 242 bytes
 */
/*
 *Function Lora_getDataRate
 *Gets data rate from module
 */
enum rn2903_datarate rn2903_get_data_rate(void){
	char response[15];
	rn2903_send_command(COM_GET_DR);
	rn2903_get_response(response, sizeof(response));
	//dbg_printSS("DR:",response);
	if (response[0]=='0')
	{
		return DR0;
	}else if (response[0]=='1')
	{
		return DR1;
	}else if (response[0]=='2')
	{
		return DR2;
	}else if (response[0]=='3')
	{
		return DR3;
	}
	return DR_ERR;
}

enum rn2903_adr_status rn2903_get_adr(void)
{
	char response[15];
	rn2903_send_command(COM_GET_ADR);
	rn2903_get_response(response, sizeof(response));
	//dbg_printSS("ADR:",response);
	if(strcmp(response,"on\r\n")==0) return ADR_TRUE;
	return ADR_FALSE;
}

enum rn2903_res_status rn2903_set_adr(bool state){
	char response[15];
	if (state)
	{
		rn2903_send_command(COM_ADR_ON);
	}else{
		rn2903_send_command(COM_ADR_OFF);
	}
	rn2903_get_response(response, sizeof(response));
	//dbg_prints(response);
	if(strcmp(response,"ok\r\n")==0) return RN_OK;
	return RN_ERR;
}

//TODO: test delay rx1
enum rn2903_res_status rn2903_set_rxdelay1(void)
{
	return rn2903_send_simple_command("mac set rxdelay1 1000\r\n",RESP_REQUEST_SUCCESS);	// save config
}

enum rn2903_res_status rn2903_mac_save(void)
{	
	return rn2903_send_simple_command(COM_SAVE,RESP_REQUEST_SUCCESS);	// save config		
}

/*
 *Function Lora_setDataRate
 *Sets data rate from module
 */
enum rn2903_res_status rn2903_set_data_rate(enum rn2903_datarate dr){
	uint8_t result = RN_ERR;
	switch(dr){
		case DR0:
		result = rn2903_send_simple_command(COM_SET_DR0,RESP_REQUEST_SUCCESS);
		break;
		case DR1:
		result = rn2903_send_simple_command(COM_SET_DR1,RESP_REQUEST_SUCCESS);
		break;
		case DR2:
		result = rn2903_send_simple_command(COM_SET_DR2,RESP_REQUEST_SUCCESS);
		break;
		case DR3:
		result = rn2903_send_simple_command(COM_SET_DR3,RESP_REQUEST_SUCCESS);
		break;
		case DR4:
		result = rn2903_send_simple_command(COM_SET_DR4,RESP_REQUEST_SUCCESS);
		break;
		case DR_ERR:
		break;
	}
	result = rn2903_send_simple_command(COM_SAVE,RESP_REQUEST_SUCCESS) | result;	// save config		
	return result;
}


/*
 *Function Lora_checkPayload
 *Checks size of the payload according to SF or DR
 */
uint8_t Lora_payloadAutoDR(char * datain){
	uint8_t dataBytes;
	uint8_t result = RN_ERR;
	dataBytes = strlen(datain)/2; //calculate the size of the message in bytes to control DR
	if(dataBytes<=11){
		result = rn2903_set_data_rate(DR0);
	}else if((dataBytes>11)||(dataBytes<=53)){
		result = rn2903_set_data_rate(DR1);
	}else if((dataBytes>53)||(dataBytes<=129)){
		result = rn2903_set_data_rate(DR2);
	}else if((dataBytes>129)||(dataBytes<=242)){
		result = rn2903_set_data_rate(DR3);
	}else if(dataBytes>242){
		result = RN_ERR;
	}
	return result;
}

enum rn2903_res_status rn2903_add_uplink_err(uint8_t * err_uplink)
{
	
	*err_uplink += 1;
	if (*err_uplink >=MAX_UPLNK_ERROR)
	{
		dbg_prints("MAX_ERR_UPLNK");
		return RN_ERR;
	}
	dbg_printSn("ERR_UPLNK:",*err_uplink);
	return RN_OK;

}

enum rn2903_res_status rn2903_clear_uplink_err(uint8_t * err_uplink)
{
	
	* err_uplink = 0;
	return RN_OK;
}

enum rn2903_res_status rn2903_add_join_err(rn2903_module * module)
{
	module->err_join += 1;
	if (module->err_join >=MAX_JOIN_ERROR)
	{
		dbg_prints("MAX_ER_JN");
		return RN_ERR;
	}
	dbg_printSn("ERR_JN:",module->err_join);
	return RN_OK;
}

enum rn2903_res_status rn2903_clear_join_err(rn2903_module * module)
{
	module->err_join = 0;
	return RN_OK;
}

