/*
 * lora_rn2903_answers.h
 *
 * Created: 28-Jun-18 3:30:29 PM
 *  Author: Diego Hinojosa
 */ 


#ifndef LORA_RN2903_ANSWERS_H_
#define LORA_RN2903_ANSWERS_H_

#define END_COMMAND			"\r\n"

/* Header of commands */
/* Commands to send without EOL */
#define MAC_TX_CNF		"mac tx cnf"
#define MAC_TX_UNCNF	"mac tx uncnf"

//TODO: revisar sin la fecha, solo la version
#define RN2903_WHO_AM_I_V098 "RN2903 0.9.8 Feb 14 2017 20:17:03\r\n"
#define RN2903_WHO_AM_I_V103 "RN2903 0.9.8 Feb 14 2017 20:17:03\r\n"

/* join commands */
#define JOIN_REQ_OTAA	"mac join otaa"END_COMMAND
#define JOIN_REQ_ABP	"mac join abp"END_COMMAND
/* join responses */
#define ANSW_TX2_JOIN_ACCEPTED "accepted"END_COMMAND

/* commands to set parameters */
#define MAC_SET_DEVADDR "mac set devaddr "
#define MAC_SET_DEVEUI  "mac set deveui "
#define MAC_SET_APPEUI  "mac set appeui "
#define MAC_SET_NWKSKEY "mac set nwkskey "
#define MAC_SET_APPSKEY "mac set appskey "
#define MAC_SET_APPKEY  "mac set appkey "
#define MAC_SET_PWRIDX  "mac set pwridx " //5,7,8,9,10

#define MAC_SET_DR			"mac set dr " //0-4
#define MAC_SET_ADR_ON		"mac set adr on"END_COMMAND		// adr on
#define MAC_SET_ADR_OFF		"mac set adr off"END_COMMAND	// adr off

#define MAC_CHANNEL_SET     "mac set ch status "  //  <channel ID>  <status>
#define MAC_ACTIVE_CHAN		"on"
#define MAC_DEACTIVE_CHAN	"off"

#define MAC_SET_BAT		"mac set bat " //0-254, 255 means the end device was not able to measure the battery level
#define MAC_SET_RETX	"mac set retx " //0-255

#define MAC_SAVE		"mac save\r\n"


/* First response after TX command */
#define ANSW_TX1_OK				"ok"END_COMMAND
#define ANSW_TX1_INV_PARAM		"invalid_param"END_COMMAND
#define ANSW_TX1_NOT_JOINED		"not_joined"END_COMMAND
#define ANSW_TX1_NO_FREE_CHAN	"no_free_ch"END_COMMAND
#define ANSW_TX1_SILENT			"silent"END_COMMAND
#define ANSW_TX1_FRM_CNT_ERR	"frame_counter_err_rejoin_needed"END_COMMAND
#define ANSW_TX1_BUSY			"busy"END_COMMAND
#define ANSW_TX1_MAC_PAUSED		"mac_paused"END_COMMAND
#define ANSW_TX1_INV_DATA_LEN	"invalid_data_len"END_COMMAND

/* Response after uplink transmission */
#define ANSW_TX2_OK				"mac_tx_ok"END_COMMAND
#define ANSW_TX2_RX				"mac_rx"
#define ANSW_TX2_MAC_ERR		"mac_err"END_COMMAND
#define ANSW_TX2_INV_DATA_LEN	"invalid_data_len"END_COMMAND




#endif /* LORA_RN2903_ANSWERS_H_ */