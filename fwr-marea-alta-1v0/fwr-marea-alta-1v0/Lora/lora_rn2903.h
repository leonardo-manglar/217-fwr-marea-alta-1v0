/*
 * lora_rn2903.h
 *
 * Created: 27-Jun-18 12:20:06 PM
 *  Author: Diego Hinojosa
 */ 


#ifndef LORA_RN2903_H_
#define LORA_RN2903_H_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#ifndef F_CPU
#define F_CPU 16000000UL //!< Clock Frequency
#endif
#include <util/delay.h>
#include "uart.h"
#include "serial_debug.h"
#include "lora_util_msgs.h"

/************************************************************************/
/*				ERRORS                                                  */
/************************************************************************/
#define MAX_JOIN_ERROR	10
#define MAX_UPLNK_ERROR	5
#define MAX_MAC_ERROR   3

/************************************************************************/
/*				DEFINES                                                 */
/************************************************************************/
#define END_COMMAND			"\r\n"
#define COM_GET_VERSION		"sys get ver\r\n"
#define COM_RESET			"sys reset\r\n"
#define COM_FACTORY_RESET	"sys factoryRESET\r\n"
#define COM_SAVE			"mac save\r\n"
#define COM_JOIN_OTAA		"mac join otaa\r\n"
#define COM_JOIN_ABP		"mac join abp\r\n"
#define COM_GET_STATUS		"mac get status\r\n"
#define COM_GET_DR			"mac get dr\r\n"
#define COM_GET_ADR			"mac get adr\r\n"

#define COM_GET_HWEUI		"sys get hweui\r\n"

// to otaaa 
#define COM_SET_DEVEUI		"mac set deveui"
#define COM_SET_APPEUI		"mac set appeui"
#define COM_SET_APPKEY		"mac set appkey"
// to abp
#define COM_SET_NWKSKEY		"mac set nwkskey"
#define COM_SET_APPSKEY		"mac set appskey"
#define COM_SET_DEVADDR		"mac set devaddr"

#define COM_PUBLIC_SYNC     "mac set sync 34\r\n"
#define COM_PRIVATE_SYNC    "mac set sync 12\r\n"
#define COM_ADR_ON			"mac set adr on\r\n"
#define COM_ADR_OFF			"mac set adr off\r\n"

#define COM_SET_DR0			"mac set dr 0\r\n" 
#define COM_SET_DR1			"mac set dr 1\r\n" 
#define COM_SET_DR2			"mac set dr 2\r\n" 
#define COM_SET_DR3			"mac set dr 3\r\n"
#define COM_SET_DR4			"mac set dr 4\r\n"



#define COM_CNF_SEND		"mac tx cnf"	
#define COM_UNCNF_SEND		"mac tx uncnf"

// Block channels
#define COM_CHANNEL_SET     "mac set ch status"  //  <channel ID>  <status>
#define COM_ACTIVE_CHAN		"on"
#define COM_DEACTIVE_CHAN	"off"

// Response after join 
#define RESP_JOIN_SUCCESS			"accepted\r\n"
#define RESP_JOIN_FAILED			"denied\r\n"

// Response after send command
#define RESP_REQUEST_SUCCESS		"ok\r\n"
#define RESP_INVALID_PARAMETER		"invalid_param\r\n"
#define RESP_NOT_JOINED				"not_joined\r\n"
#define RESP_NO_FREE_CHANNEL		"no_free_ch\r\n"
#define RESP_SILENT						"silent\r\n"
#define RESP_FRAME_COUNTER			"frame_counter_err_rejoin_needed\r\n"
#define RESP_BUSY					"busy\r\n"
#define RESP_MAC_PAUSED				"mac_paused\r\n"
#define RESP_ERR_DATA_LEN			"invalid_data_len\r\n"

//Response after uplink
#define RESP_SEND_DATA_MAC_SUCCES	"mac_tx_ok\r\n"
#define RESP_RECEIVE_DATA			"mac_rx"				
#define RESP_MAC_ERROR				"mac_err\r\n"
#define RESP_ERR_DATA_LEN			"invalid_data_len\r\n"


/************************************************************************/
/*               ENUMs and STRUCTs                                       */
/************************************************************************/

enum rn2903_res_status {
	RN_OK,
	RN_ERR
};

enum rn2903_wait_mode {
	RN_WAIT_NORMAL,
	RN_WAIT_SLEEP
};

enum rn2903_join_mode{
	RN_OTAA,
	RN_ABP
};

//Responses after join request
enum join_responses{
	JOIN_SUCCESS = 0,
	JOIN_DENIED = 1,
	JOIN_ERROR
};


enum send_msg_responses{
	UPLNK_OK				= 0,
	UPLNK_DATA_OK			= 1,
	ERROR_INVALID_PARAM		= 2,
	ERROR_NOT_JOINED		= 3,
	ERROR_NO_FREE_CHAN		= 4,
	ERROR_SILENT			= 5,
	ERROR_FRAME_COUNT_ERR	= 6,
	ERROR_BUSY				= 7,
	ERROR_MAC_PAUSED		= 8,
	ERROR_INVALID_DATA_LEN	= 9,
	ERROR_MAC_ERROR			= 10,
	ERROR_UNKNOW			= 11
};

enum rn2903_datarate{
	DR0=0,
	DR1,
	DR2,
	DR3,
	DR4,
	DR_ERR
};

enum rn2903_adr_status{
	ADR_TRUE,
	ADR_FALSE
};

// Structure to LORAWAN SYSTEM
typedef struct {
	char downlink[20];
	char uplink[50]; //11 bytes to DR0
	enum rn2903_datarate dr;
	enum rn2903_adr_status adr;
	uint8_t err_join;
	uint8_t err_uplink;
 	char deveui[17];
	char appeui[17];
	char appkey[33];
	char devaddr[9];
} rn2903_module;

struct node_config {
	enum rn2903_datarate datarate;
	enum rn2903_join_mode join_mode;
	enum rn2903_wait_mode wait_mode;
};

enum rn2903_conn_status{
	RN_CONN_OK,
	RN_CONN_ERR
};

enum rn2903_join_res_status{
	RN_JOIN_ACCEPTED,
	RN_JOIN_INV_PARAM,
	RN_JOIN_KEY_NOT_INIT,
	RN_JOIN_NO_FREE_CH,
	RN_JOIN_SILENT,
	RN_JOIN_BUSY,
	RN_JOIN_MAC_PAUSED,
	RN_JOIN_DENIED,
	RN_JOIN_ERR
};

enum rn2903_join_status{
	RN_JOINED,
	RN_NOT_JOINED
};
	

enum rn2903_res_status rn2903_set_deveui(const char * deveui_str);
enum rn2903_res_status rn2903_set_appkey(const char * appkey_str);
enum rn2903_res_status rn2903_set_appeui(const char * appeui_str);
enum rn2903_res_status rn2903_set_devaddr(const char * devaddr_str);
enum rn2903_adr_status rn2903_get_adr(void);
enum rn2903_res_status rn2903_set_adr(bool state);
enum rn2903_res_status rn2903_mac_save(void);

enum rn2903_res_status rn2903_send_simple_command(const char * command, const char * resp_expect);
enum rn2903_res_status rn2903_send_simple_command_param( const char *  command, const char *  param1);
enum rn2903_res_status rn2903_send_command_double_res(const char * command, const char * resp_expected1,const char * resp_expected2);

enum rn2903_res_status  rn2903_init_module(const rn2903_module * module);
enum rn2903_res_status rn2903_set_parameters(rn2903_module * module, const char * deveui, const char * appkey, const char * appeui);
enum rn2903_conn_status rn2903_check_module(void);
uint8_t rn2903_reset(void);
enum rn2903_res_status rn2903_reset_factory(void);
enum rn2903_res_status rn2903_set_channels(void);
enum rn2903_res_status rn2903_get_data_downlink(char * allData, rn2903_module * dest);

enum send_msg_responses rn2903_send_cnf_data(const char *fport, const char * data, rn2903_module * dest);
enum send_msg_responses rn2903_send_uncnf_data(const char *fport, const char * data, rn2903_module * dest);

enum rn2903_res_status rn2903_add_uplink_err(uint8_t * module);
enum rn2903_res_status rn2903_clear_uplink_err(uint8_t * err_uplink);
enum rn2903_res_status rn2903_add_join_err(rn2903_module * module);
enum rn2903_res_status rn2903_clear_join_err(rn2903_module * module);

void Lora_getDataServer(char * response,char * dest);

enum rn2903_join_res_status rn2903_join(void);
enum rn2903_res_status rn2903_get_status(char * status);
enum rn2903_join_status rn2903_is_join(void);
enum rn2903_res_status rn2903_set_bat(uint8_t bat_level);

/*	 DataRate
 * 		0: 		SF = 10, BW = 125 kHz, BitRate =   980 bps, max payload = 11  bytes
 *  	1: 		SF = 9,  BW = 125 kHz, BitRate =  1760 bps, max payload = 53  bytes
 *  	2: 		SF = 8,  BW = 125 kHz, BitRate =  3125 bps, max payload = 129 bytes
 *  	3: 		SF = 7,  BW = 125 kHz, BitRate =  5470 bps, max payload = 242 bytes
 */
enum rn2903_datarate rn2903_get_data_rate(void);


enum rn2903_res_status rn2903_set_data_rate(enum rn2903_datarate dr);


uint8_t Lora_payloadAutoDR(char * datain);





#endif /* LORA_RN2903_H_ */