/*
 * uart.h
 *
 * Created: 18-Jul-18 11:45:41 AM
 *  Author: Diego Hinojosa
 */ 


#ifndef UART_H_
#define UART_H_

#ifdef F_CPU
#else
#define F_CPU 16000000UL
#endif

#define CARRIGE_RETURN	13
#define LINE_FEED		10

#define UART_OK		0
#define UART_ERR		1

#include <avr/io.h>
#include <string.h>
#include "interrupt.h"
#include "ring_buffer.h"

#define ATMEGA_USART0
#define ATMEGA_USART1
#define ATMEGA_USART2

#define UART0_RECEIVE_INTERRUPT		USART0_RX_vect
#define UART0_TRANSMIT_INTERRUPT	USART0_UDRE_vect
#define UART0_STATUS				UCSR0A
#define UART0_CONTROL				UCSR0B
#define UART0_DATA					UDR0
#define UART0_UDRIE					UDRIE
#define UART0_UDRE					UDRE
#define UART0_RXC					RXC
#define UART0_UCSZ0					UCSZ0
#define UART0_UCSZ1					UCSZ1
#define UART0_TXEN					TXEN
#define UART0_RXEN					RXEN

#define UART1_RECEIVE_INTERRUPT		USART1_RX_vect
#define UART1_TRANSMIT_INTERRUPT	USART1_UDRE_vect
#define UART1_STATUS				UCSR1A
#define UART1_CONTROL				UCSR1B
#define UART1_DATA					UDR1
#define UART1_UDRIE					UDRIE
#define UART1_UDRE					UDRE
#define UART1_RXC					RXC
#define UART1_UCSZ0					UCSZ0
#define UART1_UCSZ1					UCSZ1
#define UART1_TXEN					TXEN
#define UART1_RXEN					RXEN

#define UART2_RECEIVE_INTERRUPT		USART2_RX_vect
#define UART2_TRANSMIT_INTERRUPT	USART2_UDRE_vect
#define UART2_STATUS				UCSR2A
#define UART2_CONTROL				UCSR2B
#define UART2_DATA					UDR2
#define UART2_UDRIE					UDRIE
#define UART2_UDRE					UDRE
#define UART2_RXC					RXC
#define UART2_UCSZ0					UCSZ0
#define UART2_UCSZ1					UCSZ1
#define UART2_TXEN					TXEN
#define UART2_RXEN					RXEN



#define BAUD_PRESCALADE(b) ((((F_CPU/16)+(b/2))/(b))-1)   // most updated!


/************************************************************************/
/* UART 0																*/
/************************************************************************/
#ifdef ATMEGA_USART0

/* 
 *Function initUSART0
 * Init the uart0 in atmega and setting the configuration
 * 8bit,non-parity bit, 1 stop bit [8N1]
 * You must pass the baudrate as parameter
 */
void initUSART0(unsigned int baudrate);

/*
 *Function uart0_putchar
 * This function sends a character through serial  
 */
void uart0_putchar(unsigned char c);

/*
 *Function uart0_getchar
 * This function gets a character from serial
 */
char uart0_getchar(void);

/*
 *Function uart0_getchar_t
 * This function gets a character but has a timeout to return it.
 * 16MHz /overflow=915,tcnt0=135
 * 8MHz /overflow=458, tcnt0=195
 */
char uart0_getchar_t(void);

/*
 *Function uart0_getchar_t
 * This function gets a character but has a timeout to return it.
 */
char uart0_getchar_t40(void);

/*
 *Function uart0_putstring
 * This function sends the string through the serial port
 */
void uart0_putstring(const char *string);

/*
 *Function uart0_putstring
 * This function sends the string through the serial port
 */
void uart0_putstringL(const char *string, uint32_t data_size);

/*
 *Function uart0_getResponse
 * This function gets the string from the serial port
 * Buffers the string until it finds the delimiter CR+LF or \r\n
 */
void uart0_getResponse(char * response, uint8_t sizedata);

/*
 *Function uart0_getResponse_t
 * This function gets the string from the serial port but has a timeout to find the delimiter CR+LF
 * Buffers the string until it finds the delimiter CR+LF or \r\n
 */
void uart0_getResponse_t(char * response, uint8_t sizedata);

/*
 *Function uart0_getResponse_t
 * This function gets the string from the serial port but has a timeout to find the delimiter CR+LF
 * Buffers the string until it finds the delimiter CR+LF or \r\n
 */
void uart0_getResponse_t40(char * response, uint8_t sizedata);
#endif // ATMEGA_USART0

/************************************************************************/
/* UART 1																*/
/************************************************************************/

#if defined(ATMEGA_USART1)

void initUSART1(unsigned int baudrate);
void uart1_putchar(unsigned char c);
char uart1_getchar();

/*
*	TIMER1 16-bit/F_CPU:8mhz/delay:15sec/over_fl:2,tcnt0:51651
*/
char uart1_getchar_t();
void uart1_putstring(const char *string);

/*
 *Function uart1_putstring
 * This function sends the string through the serial port
 */
void uart1_putstringL(const char *string, uint32_t data_size);
void uart1_getResponse(char * resp, int datasize);
void uart1_getResponse_t(char * response, uint8_t sizedata);
#endif //ATMEGA_USART1

/************************************************************************/
/* UART 2																*/
/************************************************************************/

#if defined(ATMEGA_USART2)
void initUSART2(unsigned int baudrate);
void uart2_enableInterrupt(void);
void uart2_disableInterrupt(void);
void uart2_putchar(unsigned char c);
char uart2_getchar(void);
char uart2_getchar_t(void);
void uart2_putstring(const char *string);

/*
 *Function uart0_putstring
 * This function sends the string through the serial port
 */
void uart2_putstringL(const char *string, uint32_t data_size);
void uart2_getResponse(char * resp, int datasize);
void uart2_getResponse_t(char * response, uint8_t sizedata);

#endif //ATMEGA_USART2


/**
 * Function inString
 * \brief Checks if a string is inside another, particularly in the response.
 * Returns SUCCESS or ERROR
 */
uint8_t inString(const char * str1, const char * str2);

/**
 * \brief Checks if two string are the same.
 * \params str1 is first string to compare
 * \params str2 is second string to compare
 * \return zero if the string are the same
 */
uint8_t checkResponse(const char * str1,const char *str2);

/**
 * \brief Check if response is true or false and print in usart0.
 * \param response Analyzes if the response is true or false and print result
 * \return SUCCESS or ERROR in string 
 */
void checkBool(uint8_t response);

/**
 * Function setString
 * \brief Sets a string in a target variable
 */
void setString(char * dest, char * data);

uint8_t testResponse(void);

uint8_t testResponseLora(void);


#endif /* UART_H_ */