/*
 * ring_buffer.h
 *
 * Created: 29-Sep-17 5:04:26 PM
 *  Author: Diego Hinojosa
 */ 


#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_

#include <avr/io.h>
#include <stdbool.h>
#include <string.h>

#define RING_BUFFER_SIZE	48
#define NUM_OF_ELEMENT		(RING_BUFFER_SIZE-1)

typedef struct{
	uint8_t head;
	uint8_t tail;
	uint8_t data[RING_BUFFER_SIZE] ;
	uint8_t num_words;
	bool over;
}ringbuff_t;


void RB_init(ringbuff_t *  rb);

uint8_t RB_isFull(ringbuff_t * rb);

uint8_t RB_isEmpty(ringbuff_t * rb);

bool RB_enque(ringbuff_t * rb, uint8_t datain);

bool RB_deque(ringbuff_t*  rb, uint8_t *  dataout);

void RB_getString(ringbuff_t * rb, char * data);

uint8_t RB_available(ringbuff_t * rb);

void RB_flushSerial(ringbuff_t * rb);



#endif /* RING_BUFFER_H_ */