/*
 * uart.c
 *
 * Created: 18-Jul-18 11:45:51 AM
 *  Author: Diego Hinojosa
 */ 
#include "uart.h"

/************************************************************************/
/* UART 0																*/
/************************************************************************/
#ifdef ATMEGA_USART0

/* 
 *Function initUSART0
 * Init the uart0 in atmega and setting the configuration
 * 8bit,non-parity bit, 1 stop bit [8N1]
 * You must pass the baudrate as parameter
 */
void initUSART0(unsigned int baudrate)
{
// 	DDRD &= ~(1 << 0);		// PD0 input
// 	PORTD &= ~(1 << 0);		// PD0 pull-off
// 	DDRD |= 1 << 1;			// PD1 output
// 	PORTD &= ~(1 << 1);		// PD1 pull-off

	/* Enable USART0 */
	PRR0 &= ~(1 << PRUSART0);
	
	UBRR0H = (BAUD_PRESCALADE(baudrate) >> 8);		// Load upper 8 - bits of the baud rate value into the HIGH byte of the UBRR register
	UBRR0L = BAUD_PRESCALADE(baudrate);				// Load lower 8 - bits of the baud rate value into the LOW byte of the UBRR register
	//UART0_STATUS |= _BV(U2X0);							// double transmission speed
	UCSR0B |= _BV(UART0_TXEN) | _BV(UART0_RXEN);				// Enable interruptions tx and rx
	UCSR0C |= _BV(UART0_UCSZ1) | _BV(UART0_UCSZ0) ;			// Use 8-bit character sizes, and select UCRSC register
	//UART1_STATUS |= _BV(RXCIE0);						// Enable usart receive complete interrupt
}

/*
 *Function uart0_putchar
 * This function sends a character through serial  
 */
void uart0_putchar(unsigned char c){
	while((UCSR0A & _BV(UDRE)) == 0) {};	
	UDR0 = c;
}

/*
 *Function uart0_getchar
 * This function gets a character from serial
 */
char uart0_getchar(){
	while( !(UCSR0A & _BV(RXC))){};
	return UART0_DATA;
}

/*
 *Function uart0_getchar_t
 * This function gets a character but has a timeout to return it.
 * 16MHz /overflow=915,tcnt0=135
 * 8MHz /overflow=458, tcnt0=195
 */
char uart0_getchar_t(){
	timer0_init();
    while( !(UCSR0A & _BV(RXC))){
		 if(timer0_overflow_count >= 458){		// timeout 15 seconds
			 if(TCNT0>=195){
				timer0_reset();				 
				//uart0_putstring(">TIMEOUT\r\n"); //debug
				return 0;				 
			 } 
		 }
    }
	timer0_reset();
    return UART0_DATA;
}



/*
 *Function uart0_getchar_t
 * This function gets a character but has a timeout to return it.
 */
char uart0_getchar_t40(){
	timer0_init();
    while( !(UART0_STATUS & _BV(UART0_RXC))){
		 if(timer0_overflow_count >= 2441){		// timeout 40 seconds
			 if(TCNT0>=104){
				 timer0_reset();
				 return 0;				 
			 } 
		 }
    }
	timer0_reset();
    return UART0_DATA;
}

/*
 *Function uart0_putstring
 * This function sends the string through the serial port
 */
void uart0_putstring(const char *string){
	while(*string){
		uart0_putchar(*string);
		string++;
	}
}

/*
 *Function uart0_putstring
 * This function sends the string through the serial port
 */
void uart0_putstringL(const char *string, uint32_t data_size){
	uint32_t i;
	for(i=0;i<data_size;i++){
		uart0_putchar(*string);
		string++;
	}
}

/*
 *Function uart0_getResponse
 * This function gets the string from the serial port
 * Buffers the string until it finds the delimiter CR+LF or \r\n
 */
void uart0_getResponse(char * response, uint8_t sizedata){
	int i= 0;
	memset(response, 0, sizedata);

	while(1){

		char c = uart0_getchar();

		if(c=='\r'){
			response[i] = '\r';
			response[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else{
			
			response[i] = c;
			i++;
		}
	}
}


/*
 *Function uart0_getResponse_t
 * This function gets the string from the serial port but has a timeout to find the delimiter CR+LF
 * Buffers the string until it finds the delimiter CR+LF or \r\n
 */
void uart0_getResponse_t(char * response, uint8_t sizedata){
	int i= 0;
	memset(response, 0, sizedata);
	
	while(1){
		char c = uart0_getchar_t();
		
		if(c=='\r'){
			response[i] = '\r';
			response[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else if(c==0){
			response[0] ='\r';
			response[1] ='\n';
			break;
		}
		else{
			response[i] = c;
			i++;
		}
	}
}

/*
 *Function uart0_getResponse_t
 * This function gets the string from the serial port but has a timeout to find the delimiter CR+LF
 * Buffers the string until it finds the delimiter CR+LF or \r\n
 */
void uart0_getResponse_t40(char * response, uint8_t sizedata){
	int i= 0;
	memset(response, 0, sizedata);
	
	while(1){
		char c = uart0_getchar_t40();
		
		if(c=='\r'){
			response[i] = '\r';
			response[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else if(c==0){
			//uart0_putstring("LISTO\n"); //debug
			response[0] ='\r';
			response[1] ='\n';
			break;
		}
		else{
			response[i] = c;
			i++;
		}
	}
}
#endif // ATMEGA_USART0

/************************************************************************/
/* UART 1																*/
/************************************************************************/

#if defined(ATMEGA_USART1)

void initUSART1(unsigned int baudrate)
{		
// 	DDRD &= ~(1 << 2);		// PD2 input
// 	PORTD &= ~(1 << 2);		// PD2 pull-off
// 	DDRD |= 1 << 3;			// PD3 output
// 	PORTD &= ~(1 << 3);		// PD3 pull-off

 	/* Enable USART1 */
	PRR0 &= ~(1 << PRUSART1);
		
	UBRR1H = (BAUD_PRESCALADE(baudrate) >> 8);		// Load upper 8 - bits of the baud rate value into the HIGH byte of the UBRR register
	UBRR1L = BAUD_PRESCALADE(baudrate);				// Load lower 8 - bits of the baud rate value into the LOW byte of the UBRR register
	//UCSR0A |= _BV(U2X0);							// double transmission speed
	UCSR1B |=  _BV(UART1_TXEN) | _BV(UART1_RXEN);				// Enable interruptions tx and rx
	UCSR0C |= _BV(UART1_UCSZ1) | _BV(UART1_UCSZ0) ;			// Use 8-bit character sizes, and select UCRSC register

	//UCSR1B |= _BV(RXCIE);						// Enable usart receive complete interrupt
	//sei();
}

void uart1_putchar(unsigned char c){
	while((UCSR1A & _BV(UDRE)) == 0) {};
	UDR1 = c;
}

char uart1_getchar(){
	while( !(UCSR1A & _BV(UART1_RXC))){};
	return UART1_DATA;
}

/*
*	TIMER1 16-bit/F_CPU:8mhz/delay:15sec/over_fl:2,tcnt0:51651
*/
char uart1_getchar_t(){
	timer1_init();
	while( !(UCSR1A & _BV(UART1_RXC))){
		if(timer1_overflow_count >= 1){
			if(TCNT1>=58594){
				timer1_reset();
				//uart2_putstring(">TIMEOUT\r\n"); //debug
				return 0;
			}
		}
	}
	timer1_reset();
	return UDR1;
}


void uart1_putstring(const char *string){
	while(*string){
		uart1_putchar(*string);
		string++;
	}
}

/*
 *Function uart1_putstring
 * This function sends the string through the serial port
 */
void uart1_putstringL(const char *string, uint32_t data_size){
	uint32_t i;
	for(i=0;i<data_size;i++){
		uart1_putchar(*string);
		string++;
	}
}




void uart1_getResponse(char * resp, int datasize){
	int i= 0;
	memset(resp, 0, datasize);

	while(1){

		char c = uart1_getchar();

		if(c=='\r'){
			resp[i] = '\r';
			resp[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else{
			
			resp[i] = c;
			i++;
		}
	}
}


void uart1_getResponse_t(char * response, uint8_t sizedata){
	int i= 0;
	memset(response, 0, sizedata);
	
	while(1){
		char c = uart1_getchar_t();
		if(c=='\r'){
			response[i] = '\r';
			response[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else if(c==0){
			response[0] ='\r';
			response[1] ='\n';
			break;
		}
		else{
			response[i] = c;
			i++;
		}
	}
}



#endif //ATMEGA_USART1

/************************************************************************/
/* UART 2																*/
/************************************************************************/

#if defined(ATMEGA_USART2)

void initUSART2(unsigned int baudrate)
{	
// 	DDRE &= ~(1 << 2);		// PD2 input
// 	PORTE &= ~(1 << 2);		// PD2 pull-off
// 	DDRE |= 1 << 3;			// PD3 output
// 	PORTE &= ~(1 << 3);		// PD3 pull-off

 	/* Enable USART2 */
	PRR2 &= ~(1 << PRUSART2);
	
	UBRR2H = (BAUD_PRESCALADE(baudrate) >> 8);		// Load upper 8 - bits of the baud rate value into the HIGH byte of the UBRR register
	UBRR2L = BAUD_PRESCALADE(baudrate);				// Load lower 8 - bits of the baud rate value into the LOW byte of the UBRR register
	//UCSR0A |= _BV(U2X0);							// double transmission speed
	UCSR2B |= _BV(TXEN) | _BV(RXEN);				// Enable interruptions tx and rx
	UCSR2C |= _BV(UCSZ1) | _BV(UCSZ0) ;			// Use 8-bit character sizes, and select UCRSC register
	
	UCSR2B |= _BV(RXCIE);						// Enable usart receive complete interrupt
	sei();
	
}

void uart2_enableInterrupt(){
	UCSR2B |= _BV(RXCIE);						// Enable usart receive complete interrupt
}

void uart2_disableInterrupt(){
	UCSR2B &= ~(_BV(RXCIE));						// disable usart receive complete interrupt
}

void uart2_putchar(unsigned char c){
	while((UCSR2A & _BV(UDRE)) == 0) {};
	UDR2 = c;
}

char uart2_getchar(){
	while( !(UCSR2A & _BV(RXC))){};
	return UDR2;
}

char uart2_getchar_t(){
	timer3_init();
	while( !(UCSR2A & _BV(RXC))){
		if(timer3_overflow_count >= 610){
			if(TCNT3>=90){
				timer3_reset();
				//uart2_putstring(">TIMEOUT\n"); //debug
				return 0;
			}
		}
	}
	timer3_reset();
	return UDR2;
}


void uart2_putstring(const char *string){
	while(*string){
		uart2_putchar(*string);
		string++;
	}
}

/*
 *Function uart0_putstring
 * This function sends the string through the serial port
 */
void uart2_putstringL(const char *string, uint32_t data_size){
	uint32_t i;
	for(i=0;i<data_size;i++){
		uart2_putchar(*string);
		string++;
	}
}




void uart2_getResponse(char * resp, int datasize){
	int i= 0;
	memset(resp, 0, datasize);

	while(1){

		char c = uart2_getchar();

		if(c=='\r'){
			resp[i] = '\r';
			resp[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else{
			
			resp[i] = c;
			i++;
		}
	}
}


void uart2_getResponse_t(char * response, uint8_t sizedata){
	int i= 0;
	memset(response, 0, sizedata);
	
	while(1){
		char c = uart2_getchar_t();
		
		if(c=='\r'){
			response[i] = '\r';
			response[i+1] = '\n';
			break;
		}
		else if(c=='\n'){
			i=0;
		}
		else if(c==0){
			//uart0_putstring("LISTO\n"); //debug
			response[0] ='\r';
			response[1] ='\n';
			break;
		}
		else{
			response[i] = c;
			i++;
		}
	}
}

#endif //ATMEGA_USART2


/**
 * Function inString
 * \brief Checks if a string is inside another, particularly in the response.
 * Returns SUCCESS or ERROR
 */
uint8_t inString(const char * str1, const char * str2){
	if(strstr(str2, str1) != NULL) return UART_OK;
	else return UART_ERR;	
}

/**
 * \brief Checks if two string are the same.
 * \params str1 is first string to compare
 * \params str2 is second string to compare
 * \return zero if the string are the same
 */
uint8_t checkResponse(const char * str1,const char *str2){
	return strcmp(str1 , str2);
}
/**
 * \brief Check if response is true or false and print in usart0.
 * \param response Analyzes if the response is true or false and print result
 * \return SUCCESS or ERROR in string 
 */
void checkBool(uint8_t response){
	if(response){
		uart0_putstring("Error\n");
	}else{
		uart0_putstring("Success\n");
	}
}

/**
 * Function setString
 * \brief Sets a string in a target variable
 */
void setString(char * dest, char * data){
	strcpy(dest, data);
}

uint8_t testResponse(){
	char response[40];
	uart1_putstring("What's your name?\r\n");	// send command
	uart1_getResponse_t(response, sizeof(response));				// wait response
	uart1_putstring(response);	// send command
	if (inString("Diego",response)==0){
		return UART_OK;
	}else{
		//RN2483
		return UART_ERR;
	}
}

uint8_t testResponseLora(){
	char response[40];
	uart0_putstring("sys get ver\r\n");	// send command
	uart0_getResponse_t(response, sizeof(response));				// wait response
	uart0_putstring(response);	// send command
	if (inString("RN2903",response)==0){
		return UART_OK;
		}else{
		//RN2483
		return UART_ERR;
	}
}
