/*
 * ring_buffer.c
 *
 * Created: 18-Jul-18 12:23:41 PM
 *  Author: Diego Hinojosa
 */ 
#include "ring_buffer.h"

void RB_init(ringbuff_t *  rb){
	memset(rb->data, 0, RING_BUFFER_SIZE);
	rb->head = 0;
	rb->tail = 0;
	rb->num_words = 0;
	rb->over = false;
}

uint8_t RB_isFull(ringbuff_t * rb){
	return (((rb->head+ 1 )%RING_BUFFER_SIZE)==rb->tail);
}
uint8_t RB_isEmpty(ringbuff_t * rb){
	return (rb->tail == rb->head);
}

bool RB_enque(ringbuff_t * rb, uint8_t datain) {
	uint8_t isFull = RB_isFull(rb);
	if(!isFull){
		rb->data[rb->head] = datain;
		rb->head++;
		rb->head %= RING_BUFFER_SIZE;
	}
	return isFull;
}

bool RB_deque(ringbuff_t*  rb, uint8_t *  dataout) {
	uint8_t isEmpty = RB_isEmpty(rb);
	if(!isEmpty){
		*dataout = rb->data[rb->tail];
		rb->tail++;
		rb->tail %= RING_BUFFER_SIZE;
	}
	return isEmpty;
}


void RB_getString(ringbuff_t * rb, char * data){
	memset(data,0,sizeof(*data));
	uint8_t temp_data = '\0';
	int i = 0;
	while(!RB_deque(rb,&temp_data)){
		data[i] = temp_data;
		//usart_putchar(USART_XB,temp_data); //debug
		//usart_putchar(USART_XB,data[i]);	//debug
		if(temp_data=='\n'){
			if(rb->num_words!=0){
				rb->num_words-=1;
			}
			break;
		}
		i++;
	}
}


uint8_t RB_available(ringbuff_t * rb){
	return !RB_isEmpty(rb);
}

void RB_flushSerial(ringbuff_t * rb){
	RB_init(rb);
}



