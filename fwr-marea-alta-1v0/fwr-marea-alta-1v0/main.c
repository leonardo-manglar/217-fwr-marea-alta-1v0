/*
 * fwr-marea-alta-1v0.c
 *
 * Created: 18-03-2020 10:39:59
 * Author : hardw
 */ 

/*************************************
***************DEFINES****************
*************************************/
#define F_CPU 16000000UL

#define DEVEUI "e95599da17811ab6"						//	LoRa deveui
#define APPEUI "e95599da17811ab6"						//	LoRa appeui
#define APPKEY "c2228f33f4f0b05c8143b7623c08ef1e"		//	LoRa appkey
#define PERIOD_TX_LORA 300								//	Transmission Period in seconds

#define CHIP_SELECT_1 PINB4
#define CHIP_SELECT_2 PINB1

#define DATA_READY_1 PINB2
#define DATA_READY_2 PINB0


/*************************************
***************INCLUDES***************
*************************************/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart.h"
#include "spi.h"
#include "lora_rn2903.h"
#include "max31865.h"
#include "adc.h"


void get_readings();

/****************************************
************PRIV VARIABLES*************
****************************************/
unsigned int temp1, temp2;
char char_temp1, char_temp2;
char str[5];
char battery;
int status;			//	AC Line status

/****************************************
**************INT VECTORS****************
****************************************/
ISR(INT0_vect)
{
	get_readings();	
}


ISR(INT1_vect)
{
	get_readings();
}



/****************************************
*************MAIN FUNCTION***************
****************************************/

int main(void)
{
	
	/****************************************
	***************INIT GPIOS****************
	****************************************/
	
	// PD5_LED_DBG1 will be used to indicate LoRa join successful
	DDRD |= (1<<PIND5);		//	Set PD5 to output
	PORTD |= (1<<PIND5);	//	Initialize PD5_LED_DBG1 OFF
	
	//	PB3 to Enable AFE Supply
	DDRB |= (1<<PINB3);		//	Set PB3 to output
	PORTB |= (1<<PINB3);	//	Set PB3 High to enable AFE supply
	
	
	//	PA0_EN_BAT_READ to output
	DDRA |= (1<<PINA0);
	
	
	/****************************************
	************INIT INTERRUPTS**************
	****************************************/
	EICRA |= ((1<<ISC00) | (1<<ISC10));			//	Interrupt on any logical change of INT0 and INT1 pins
	EIMSK |= ((1<<INT0) | (1<<INT1));			//	Enable INT0 & INT1 external interrupts
	
	/****************************************
	*************INIT PERIPHERALS************
	****************************************/
	
    initUSART0(57600);						//	Init UART0 to communicate with RN2903
	max31865_slave_init(CHIP_SELECT_1);		//	Init RTD Sensor 1
	max31865_slave_init(CHIP_SELECT_2);		//	Init RTD Sensor 2
	SPI0_MasterInit();						//	Init SPI to communicate with RTD ADCs
	adc_init_single_conv();				//	Init ADC channel 7 to read battery level
	
		
	
	/****************************************
	*************CONFIGURE RN2903************
	****************************************/
	while(rn2903_reset_factory());
	_delay_ms(1000);
	rn2903_set_channels();
	_delay_ms(100);
	rn2903_set_deveui(DEVEUI);
	_delay_ms(100);
	rn2903_set_appeui(APPEUI);
	_delay_ms(100);
	rn2903_set_appkey(APPKEY);
	_delay_ms(100);
	rn2903_set_adr(false);
	_delay_ms(100);
	rn2903_set_data_rate(DR4);
	_delay_ms(100);
	while(rn2903_join())
	{
		_delay_ms(1000);
	}
	PORTD &= ~(1<<PIND5);		//	PD5_LED_DBG1 ON if join accepted
	
	//sei();						//	Enable Global Interrupts
	
	
	timer0_init();
	/****************************************
	***************INFINITE LOOP*************
	****************************************/
    while (1) 
    {
		//	Temperature readings
		
		if(timer0_overflow_count >= (PERIOD_TX_LORA/0.01638))
		{
			get_readings();
			timer1_reset();
			timer0_init();
		}
			
    }
}


//	Gets temp & battery readings, sends data to server
void get_readings()
{
	
	temp1 = max31865_get_RTD_val(CHIP_SELECT_1, DATA_READY_1);
	temp2 = max31865_get_RTD_val(CHIP_SELECT_2, DATA_READY_2);
		
	char_temp1 = (char)((temp1 / 32) - 256);
	char_temp2 = (char)((temp2 / 32) - 256);
	
	
	
	/****************************************
	*********AC LINE FAULT DETECTION*********
	****************************************/
	
	//	AC Line 1 fault
	if(!(PIND & (1<<PIND2)))
	{
		if(PIND & (1<<PIND3))
		{
			status = 1;		
		}
		else
		{
			status = 3;
		}
	
	}
	
	else if (!(PIND & (1<<PIND3)))
	{
		if(PIND & (1<<PIND2))
		{
			status = 2;
		}
		else
		{
			status = 4;
		}		
	}
	
	
	//	AC Line OK
	else
	{
		status = 0;
	}
		
	//	Battery level reading
	PORTA |= (1<<PINA0);									//	Enable Batt read pin
	_delay_ms(10);
	battery = (char)(adc_single_conv_read_8_bit(7));		//	Start conversion
	PORTA &= ~(1<<PINA0);									//	Disable Batt read pin
	battery = battery*(330/(255*2.1));
	
	sprintf(str, "%s %02x%02x%02x%02x%s","mac tx cnf 1", char_temp1, char_temp2, status, battery, "\r\n");
	
	uart0_putstringL(str, strlen(str));
}

