/*
 * adc.h
 *
 * Created: 19-03-2020 15:15:45
 *  Author: hardw
 */ 

#ifndef ADC_H_
#define ADC_H_

#include <avr/io.h>

enum adc_channels {
	ADC0,
	ADC1,
	ADC2,
	ADC3,
	ADC4,
	ADC5,
	ADC6,
	ADC7
};


//	Initializes ADC in single conversion mode, single ended inputs given a channel
void adc_init_single_conv(void);


//	Starts a single conversion in single ended input mode with 8-bit resolution
unsigned char adc_single_conv_read_8_bit(int channel);


#endif /* ADC_H_ */