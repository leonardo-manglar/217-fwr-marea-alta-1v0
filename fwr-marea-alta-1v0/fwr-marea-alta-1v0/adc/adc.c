/*
 * adc.c
 *
 * Created: 19-03-2020 15:15:34
 *  Author: hardw
 */ 

#include "adc.h"

//	Initializes ADC in single conversion mode, single ended inputs given a channel
void adc_init_single_conv(void)
{
	ADMUX |= (1<<REFS0); // Set ADC reference to AVCC
	ADMUX |= (1<<ADLAR);		//	Left Align for 8-bit resolution
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);		//	Enable ADC
};

//	Starts a single conversion in single ended input mode with 8-bit resolution
unsigned char adc_single_conv_read_8_bit(int channel)
{
	unsigned char conv_val;
	ADMUX |= channel;			//	Select input channel
	ADCSRA |= (1<<ADSC);				//	Start conversion
	while(!(ADCSRA & (1<<ADSC)));		//	Wait for conversion to complete
	conv_val = ADCH;
	//ADCSRA |= (1<<ADIF);				//	Writing 1 to ADIF will clear the flag
	return conv_val;
}