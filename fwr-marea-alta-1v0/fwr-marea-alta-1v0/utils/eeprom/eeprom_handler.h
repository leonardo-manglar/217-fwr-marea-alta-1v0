/*
 * eeprom_handler.h
 *
 * Created: 30-Jul-18 1:00:52 PM
 *  Author: Diego Hinojosa
 */ 


#ifndef EEPROM_HANDLER_H_
#define EEPROM_HANDLER_H_

#include <avr/io.h>
#include <avr/eeprom.h>
#include "serial_debug.h"

void eeprom_set_string(const char * string_in, void *src_addr, const uint8_t len_string);
void eeprom_get_string( void * string_out, const void *src_addr, size_t len_string);

void eeprom_set_uint16(const uint32_t * num_in, void * src_addr);
void eeprom_get_uint16(uint32_t * num_out, const void *src_addr);

void eeprom_set_uint8(const uint8_t * num_in, void * src_addr);
void eeprom_get_uint8(uint8_t * num_out, const void *src_addr);


#endif /* EEPROM_HANDLER_H_ */