/*
 * eeprom_handler.c
 *
 * Created: 30-Jul-18 1:01:03 PM
 *  Author: Diego Hinojosa
 */ 

#include "eeprom_handler.h"

void eeprom_get_string( void * string_out, const void *src_addr, size_t len_string)
{
	eeprom_read_block(string_out, src_addr, len_string);
}

void eeprom_set_string(const char * string_in, void *src_addr, const uint8_t len_string)
{	
	eeprom_write_block(string_in,src_addr, len_string);
}

//TODO: test functions to read uint16_t
void eeprom_set_uint16(const uint32_t * num_in, void * src_addr)
{
	eeprom_write_dword(src_addr,*num_in);
}

void eeprom_get_uint16(uint32_t * num_out, const void *src_addr)
{
	*num_out = eeprom_read_dword(src_addr);	
}

void eeprom_set_uint8(const uint8_t * num_in, void * src_addr)
{
	eeprom_write_byte(src_addr,*num_in);
}

void eeprom_get_uint8(uint8_t * num_out, const void *src_addr)
{
	*num_out = eeprom_read_byte(src_addr);
}