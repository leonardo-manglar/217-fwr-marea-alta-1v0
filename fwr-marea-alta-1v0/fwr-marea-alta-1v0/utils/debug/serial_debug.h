/*
 * serial_debug.h
 *
 * Created: 18-Jul-18 11:43:10 AM
 *  Author: Diego Hinojosa
 */ 


#ifndef SERIAL_DEBUG_H_
#define SERIAL_DEBUG_H_

#include <avr/io.h>
#include <stdio.h>
#include "uart.h"

void dbg_prints(const char * string);
void dbg_printSS(const char * string1, const char * string2);
void dbg_printSh(const char * string, const uint8_t num);
void dbg_printSn(const char * string, const int num);
void dbg_printc(const char ch);
void dbg_printn(const int num);
void dbg_printh(const uint8_t num);
void dbg_printhID(const char * string,const uint8_t num);
void dbg_printU32(const uint32_t num);
void dbg_printBuffH(const uint8_t *num,const uint8_t len);
void dbg_printF(const float num);
void dbg_printSF(const char * string,const float num);

#endif /* SERIAL_DEBUG_H_ */