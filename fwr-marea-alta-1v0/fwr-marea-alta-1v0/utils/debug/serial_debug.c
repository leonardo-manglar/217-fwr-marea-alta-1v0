/*
 * serial_debug.c
 *
 * Created: 18-Jul-18 11:42:58 AM
 *  Author: Diego Hinojosa
 */ 
#include "serial_debug.h" 


void dbg_prints(const char * string){
	//To optimize memory
	while(*string){
		uart2_putchar(*string);
		string++;
	}
	uart2_putchar('\r');
	uart2_putchar('\n');
}

void dbg_printSS(const char * string1,const char * string2)
{
	char debug_msg[20]="";
	sprintf(debug_msg,"%s%s",string1,string2);
	dbg_prints(debug_msg);
}

void dbg_printSh(const char * string,const uint8_t num)
{
	char debug_msg[10]="";
	sprintf(debug_msg,"%s0x%x",string,num);
	dbg_prints(debug_msg);
}

void dbg_printSn(const char * string,const int num)
{
	while(*string){
		uart2_putchar(*string);
		string++;
	}
	char debug_msg[10]="";
	sprintf(debug_msg,"%d",num);
	dbg_prints(debug_msg);
}

void dbg_printc(const char ch){
	char debug_msg[10]="";
	sprintf(debug_msg,"%c",ch);
	dbg_prints(debug_msg);
}

void dbg_printn(const int num){
	char debug_msg[10]="";
	sprintf(debug_msg,"%d",num);
	dbg_prints(debug_msg);
}

void dbg_printh(const uint8_t num){
	char debug_msg[10]="";
	sprintf(debug_msg,"0x%x",num);
	dbg_prints(debug_msg);
}

void dbg_printhID(const char * string,const uint8_t num)
{
	char debug_msg[20]="";
	sprintf(debug_msg,"%s:0x%x",string,num);
	dbg_prints(debug_msg);
}

void dbg_printU32(const uint32_t num){
	char debug_msg[20]="";
	sprintf(debug_msg,"%"PRIu32,num);
	dbg_prints(debug_msg);
}

void dbg_printBuffH(const uint8_t *num,const uint8_t len)
{
	for(uint8_t i=0; i<len;i++){
		dbg_printh(num[i]);
	}
}

void dbg_printF(const float num)
{
	char deb_buff[20]={0};
	sprintf(deb_buff,"%f",num);
	dbg_prints(deb_buff);
}

void dbg_printSF(const char * string,const float num)
{
	char deb_buff[20]={0};
	sprintf(deb_buff,"%s%f",string,num);
	dbg_prints(deb_buff);
}