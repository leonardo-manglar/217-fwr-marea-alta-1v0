/*
 * analog_read.h
 *
 * Created: 4/29/2017 11:12:29 AM
 * Author : Diego Hinojosa
 * analog read pins to Aquila Manglar Node v0.1 
 * AQMNv0.1
 * ATmega328PB
 */ 

#ifndef ANALOG_READ_H
#define ANALOG_READ_H

#define VOLTAGE_REF		3.3 

#include <avr/io.h>
#ifndef F_CPU
#define F_CPU 16000000UL //!< Clock Frequency
#endif
#include <util/delay.h>

/** 
*  \brief This function enables Analog Digital Converter.
* To configure prescaler in a ADC must be configure ADPS bits in ADCSRA register.
*/
void ADC_initADC(void);
void ADC_enable(void);
void ADC_disable(void);

/**
*  \brief Reads analog digital port channel
* \param channel Is the channel to read
* In atmega328PB: PC[5-0] is Input channel[5-0], PC[3-2] is Input channel[7-6]
*/
uint16_t ADC_analogRead(uint8_t channel);

/**
*  \brief Converts analog input value to voltage
* \param value Is the value that has been read with ADC
*/
float ADC_voltageConverter(uint16_t value);

/**
*  \brief read temperature
*/
void ADC_readTemperature(void);

/**
*  \brief read battery level
*/
uint8_t ADC_readBatteryLevel(void);

#endif