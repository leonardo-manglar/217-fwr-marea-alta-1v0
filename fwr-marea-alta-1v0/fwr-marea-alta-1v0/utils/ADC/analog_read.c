/*
 * analog_read.c
 *
 * Created: 18-Jul-18 12:42:10 PM
 *  Author: Diego Hinojosa
 */ 
#include "analog_read.h"

/**
*  \brief This function enables Analog Digital Converter.
* To configure prescaler in a ADC must be configure ADPS bits in ADCSRA register.
*/
void ADC_initADC(void)
{
	/* Enable clock to write ADC registers */
	PRR0 &= ~(1 << PRADC);
	
	ADCSRA	|= _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);	// Set ADC prescaler to 128 - 125KHz sample rate @ 16MHz
	ADMUX	|= _BV(REFS0);								// Sets the reference AREF  [Pag.321 DATASHT]
	//ADMUX |= _BV(ADLAR);								// To left align the ADC value, reduce resolution to 8 bits.
	//ADCSRA |= _BV(ADATE);								// Free Running or auto trigger, ADATE or AFR in older versions
	ADCSRA |= _BV(ADEN);								// To enable ADC
}

void ADC_enable(void)
{
	ADCSRA |= _BV(ADEN);
}

void ADC_disable(void)
{
	ADCSRA &= ~(_BV(ADEN));		//disable ADC
}

/**
*  \brief Reads analog digital port channel
* \param channel Is the channel to read
* In atmega328PB: PC[5-0] is Input channel[5-0], PC[3-2] is Input channel[7-6]
*/
uint16_t ADC_analogRead(uint8_t channel){
	ADMUX = (ADMUX & 0xF0) | (channel & 0x0F);		//select ADC channel with safety mask
	ADCSRA |= _BV(ADSC);							// start lecture
	while( ADCSRA & _BV(ADSC))	;					// ADCS will stay high as long as the conversion is in progress, and will be cleared by hardware when the conversion is completed
	return ADCW;
}

/**
*  \brief Converts analog input value to voltage
* \param value Is the value that has been read with ADC
*/
float ADC_voltageConverter(uint16_t value)
{
	return (value*VOLTAGE_REF)/1023.0;
}

void ADC_readTemperature(void){
	ADCSRA |= _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);		// Set ADC prescaler to 128 - 125KHz sample rate @ 16MHz
	ADMUX |= _BV(REFS1) |  _BV(REFS0);	// Set ref 1.1V
	
	ADMUX |= _BV(MUX3);					// select channel
	ADCSRA |= _BV(ADATE);
	
	ADCSRA |= _BV(ADEN);				// To enable ADC
	_delay_ms(10);
	ADCSRA |= _BV(ADSC);	// start lecture
	while( ADCSRA & _BV(ADSC));
}

uint8_t ADC_readBatteryLevel(void){
	int raw_value = ADC_analogRead(7);
	if (raw_value> 920)
	{
		return 90;
	}
	else if ((raw_value>= 920)&&(raw_value> 800))
	{
		return 70;
	}
	else if ((raw_value>= 800)&&(raw_value> 700))
	{
		return 60;
	}
	else if ((raw_value>= 700)&&(raw_value> 600))
	{
		return 40;
	}else return 30;
}