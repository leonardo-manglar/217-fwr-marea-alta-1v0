/*
 * bq32000.h
 *
 * Created: 09-04-2019 12:51:44
 *  Author: dhinojosac
 */ 


#ifndef BQ32000_H_
#define BQ32000_H_

#include "i2c_master.h"
#include <util/delay.h>
#include <string.h> /* memset */

#define RTC_USE_I2C_0

/** 
 *  @brief BQ32002 Register
 */
#define RTC_QB_ADDRESSw	0x68
#define RTC_QB_ADDRESSR	0x68

#define	RTC_SECONDS_REG 0x00
#define	RTC_MINUTE_REG	0x01
#define	RTC_HOUR_REG	0x02
#define RTC_DAY_REG		0x03
#define RTC_DATE_REG	0x04
#define RTC_MOTH_REG	0x05
#define RTC_YEAR_REG	0x06
#define RTC_CAL_CFG1	0x07
#define RTC_CFG2		0x08
#define RTC_KEY1		0x20
#define RTC_KEY2		0x21
#define RTC_SFR			0x22

/*
	Constructor de dato decimal a formato BCD

*/
void RTC_dec_2_bcd(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t mins, uint8_t secs, uint8_t *RTC);
/*
	DE-Constructor de dato BCD a formato decimal
*/
void RTC_bcd_2_dec(uint8_t *RTC, uint8_t *dateTime);
/*
	Actualiza la hora actual de RTC usando rtc_constructor
*/
void RTC_set_time(uint8_t *bufferRTC);
/*
	Obtiene hora actual desde RTC usando rtc_deconstructor
*/
void RTC_get_time(uint8_t *grtc);
/*
	Funcion test que imprime en consola la hoar actual de RTC
*/
void RTC_init(void);

/*
	Funcion que comprueba si hora a configurar es correcta
*/

uint8_t rtc_check_configTIME(uint8_t *_t);




#endif /* BQ32000_H_ */