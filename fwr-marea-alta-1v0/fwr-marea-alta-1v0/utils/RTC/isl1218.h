/*
 * isl1218.h
 *
 * Created: 24-Jun-19 5:41:52 PM
 *  Author: dhinojosac
 */ 


#ifndef ISL1218_H_
#define ISL1218_H_


#include "i2c_master.h"
#include <util/delay.h>
#include <string.h> /* memset */

#define ISL1218_USE_I2C_0
#define ISL1218_ADDRESS 0x6f  //TODO: change address

/** 
 *  @brief ISL1218 Register
 */
/* Register map */
/* rtc section */
#define ISL1208_REG_SC  0x00
#define ISL1208_REG_MN  0x01
#define ISL1208_REG_HR  0x02
#define ISL1208_REG_HR_MIL     (1<<7)	/* 24h/12h mode */
#define ISL1208_REG_HR_PM      (1<<5)	/* PM/AM bit in 12h mode */
#define ISL1208_REG_DT  0x03
#define ISL1208_REG_MO  0x04
#define ISL1208_REG_YR  0x05
#define ISL1208_REG_DW  0x06
#define ISL1208_RTC_SECTION_LEN 7

/* control/status section */
#define ISL1208_REG_SR  0x07
#define ISL1208_REG_SR_ARST    (1<<7)	/* auto reset */
#define ISL1208_REG_SR_XTOSCB  (1<<6)	/* crystal oscillator */
#define ISL1208_REG_SR_WRTC    (1<<4)	/* write rtc */
#define ISL1208_REG_SR_ALM     (1<<2)	/* alarm */
#define ISL1208_REG_SR_BAT     (1<<1)	/* battery */
#define ISL1208_REG_SR_RTCF    (1<<0)	/* rtc fail */

#define ISL1208_REG_INT 0x08
#define ISL1208_REG_INT_FOBATB (1<<4)   /* frecuency output */
#define ISL1208_REG_INT_LPMODE (1<<5)   /* enable low power mode */
#define ISL1208_REG_INT_ALME   (1<<6)   /* alarm enable */
#define ISL1208_REG_INT_IM     (1<<7)   /* interrupt/alarm mode */

#define ISL1219_REG_EV  0x09
#define ISL1219_REG_EV_EVEN    (1<<4)   /* event detection enable */
#define ISL1219_REG_EV_EVIENB  (1<<7)   /* event in pull-up disable */

#define ISL1208_REG_ATR 0x0a
#define ISL1208_REG_DTR 0x0b

/* alarm section */
#define ISL1208_REG_SCA 0x0c
#define ISL1208_REG_MNA 0x0d
#define ISL1208_REG_HRA 0x0e
#define ISL1208_REG_DTA 0x0f
#define ISL1208_REG_MOA 0x10
#define ISL1208_REG_DWA 0x11
#define ISL1208_ALARM_SECTION_LEN 6

/* user section */
#define ISL1208_REG_USR1 0x12
#define ISL1208_REG_USR2 0x13
#define ISL1208_USR_SECTION_LEN 2

/* event section */
#define ISL1219_REG_SCT 0x14
#define ISL1219_REG_MNT 0x15
#define ISL1219_REG_HRT 0x16
#define ISL1219_REG_DTT 0x17
#define ISL1219_REG_MOT 0x18
#define ISL1219_REG_YRT 0x19
#define ISL1219_EVT_SECTION_LEN 6

/*
 * Convert decimal to BCD format
 */
//void ISL1218_dec_2_bcd(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t mins, uint8_t secs, uint8_t *ISL1218);

/*
 * convert BCD to decimal format
 */
//void ISL1218_bcd_2_dec(uint8_t *ISL1218, uint8_t *dateTime);

/*
 * print time
*/
//void ISL1218_print_time(uint8_t *gRTC);

/*
	Init RTC configuration
*/
void ISL1218_init(void);

/*
	set time to rtc
*/
void ISL1218_set_time(uint8_t *bufferISL1218);

/*
	get time from rtc
*/
void ISL1218_get_time(uint8_t *gISL1218);

/*
	set alarm to rtc
*/
void ISL1218_set_alarm(uint8_t *bufferISL1218);


/*
	Checks if the time is right
*/

uint8_t ISL1218_check_configTIME(uint8_t *_t);





#endif /* ISL1218_H_ */