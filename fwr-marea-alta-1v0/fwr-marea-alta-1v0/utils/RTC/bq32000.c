/*
 * bq32000.c
 *
 * Created: 09-04-2019 12:51:57
 *  Author: dhinojosac
 */ 
#include "bq32000.h"

#ifdef RTC_USE_I2C_0
#	define i2c_master_readReg	i2c0_master_readReg
#	define i2c_master_writeReg	i2c0_master_writeReg
#elif defined(LIS3DE_USE_I2C_)
#	define i2c_master_readReg	i2c1_master_readReg
#	define i2c_master_writeReg	i2c1_master_writeReg
#endif


/**
 *	@brief Save data within the RTC.
 *	Decimal format to BCD format.
 */
void RTC_dec_2_bcd(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t mins, uint8_t secs, uint8_t *RTC)
{
	RTC[0] = ((year  / 10) << 4 | year  % 10);
	RTC[1] = ((month / 10) << 4 | month % 10);
	RTC[2] = ((day  / 10) << 4 | day  % 10);
	RTC[3] = ((hour  / 10) << 4 | hour  % 10);
	RTC[4] = ((mins  / 10) << 4 | mins  % 10);
	RTC[5] = ((secs  / 10) << 4 | secs  % 10);
}

/**
 *	@brief Get data from RTC.
 *  BCD format to decimal format 
 */
void RTC_bcd_2_dec(uint8_t *RTC, uint8_t *dateTime)
{
	dateTime[0] = ((RTC[0] & 0xF0)>>4)*10 + (RTC[0] & 0x0F);
	dateTime[1] = ((RTC[1] & 0xF0)>>4)*10 + (RTC[1] & 0x0F);
	dateTime[2] = ((RTC[2] & 0xF0)>>4)*10 + (RTC[2] & 0x0F);
	dateTime[3] = ((RTC[3] & 0xF0)>>4)*10 + (RTC[3] & 0x0F);
	dateTime[4] = ((RTC[4] & 0xF0)>>4)*10 + (RTC[4] & 0x0F);
	dateTime[5] = ((RTC[5] & 0xF0)>>4)*10 + (RTC[5] & 0x0F);
}

/**
 *	@brief Set date time to RTC.
 */
void RTC_set_time(uint8_t *bufferRTC)
{
	dbg_prints("Set RTC"); //debug: show set rtc
	//TODO: Check input before set it
	
	uint8_t twiRTCSet[6];
	memset(twiRTCSet,'\0',sizeof(twiRTCSet));
	RTC_dec_2_bcd(bufferRTC[0],bufferRTC[1],bufferRTC[2],bufferRTC[3],bufferRTC[4],bufferRTC[6],twiRTCSet);

	//i2c_master_writeReg(dev->dev_id, reg_addr[0], temp_buff, temp_len);
	
	uint8_t data_in = twiRTCSet[0];
	i2c_master_writeReg(RTC_QB_ADDRESSw,RTC_YEAR_REG, &data_in, 1);
	
	data_in = twiRTCSet[1];
	i2c_master_writeReg(RTC_QB_ADDRESSw,RTC_MOTH_REG, &data_in, 1);
	
	data_in = twiRTCSet[2];
	i2c_master_writeReg(RTC_QB_ADDRESSw,RTC_DATE_REG, &data_in, 1);
	
	data_in = twiRTCSet[3];
	i2c_master_writeReg(RTC_QB_ADDRESSw,RTC_HOUR_REG, &data_in, 1);
	
	data_in = twiRTCSet[4];
	i2c_master_writeReg(RTC_QB_ADDRESSw,RTC_MINUTE_REG, &data_in, 1);
	
	data_in = twiRTCSet[5];
	i2c_master_writeReg(RTC_QB_ADDRESSw,RTC_SECONDS_REG, &data_in, 1);
}

/**
 *	Get date time from RTC.
 */
void RTC_get_time(uint8_t *grtc)
{	
	dbg_prints("Get RTC"); //debug: show get rtc
	uint8_t twi_rtc_get[6];
	char dbg_buff[50]= "";
	//printf("RTCG:");
	
	//i2c_master_readReg(dev->dev_id, reg_addr, reg_data, len);
	
	i2c_master_readReg(RTC_QB_ADDRESSw,0x06, &twi_rtc_get[0],1);
	i2c_master_readReg(RTC_QB_ADDRESSw,0x05, &twi_rtc_get[1],1);
	i2c_master_readReg(RTC_QB_ADDRESSw,0x04, &twi_rtc_get[2],1);
	i2c_master_readReg(RTC_QB_ADDRESSw,0x02, &twi_rtc_get[3],1);
	i2c_master_readReg(RTC_QB_ADDRESSw,0x01, &twi_rtc_get[4],1);
	i2c_master_readReg(RTC_QB_ADDRESSw,0x00, &twi_rtc_get[5],1);

	RTC_bcd_2_dec(twi_rtc_get,grtc);
	sprintf(dbg_buff,"#GT:%i/%i/%iT%i:%i:%i\n",grtc[0],grtc[1],grtc[2],grtc[3],grtc[4],grtc[5]);
	dbg_prints(dbg_buff);
}

/**
 *	@brief Init RTC
 */
void RTC_init()
{
	uint8_t twi_rtc_rawt[6];
	RTC_get_time( twi_rtc_rawt);	
}

/**
 *	@breif Check RTC parameters
 */
uint8_t rtc_check_configTIME(uint8_t *_t)
{

	uint8_t res = 0;
	
	if (_t[0] >= 0 && _t[0] <= 99){res += 1;}
	if (_t[1] >= 1 && _t[1] <= 12){res += 1;}
	if (_t[2] >= 1 && _t[2] <= 31){res += 1;}
	
	if (_t[3] >= 0 && _t[3] <= 23){res += 1;}
	if (_t[4] >= 0 && _t[4] <= 59){res += 1;}
	if (_t[5] >= 0 && _t[5] <= 59){res += 1;}
	
	if (res == 6)
	{
		return 1;
	}
	else
	{
		return 0;
	}
	
}