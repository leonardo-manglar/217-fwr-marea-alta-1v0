/*
 * isl1218.c
 *
 * Created: 24-Jun-19 5:41:42 PM
 *  Author: dhinojosac
 */ 
#include "isl1218.h"


#ifdef ISL1218_USE_I2C_0
#	define i2c_master_readReg	i2c0_master_readReg
#	define i2c_master_writeReg	i2c0_master_writeReg
#elif defined(ISL1218_USE_I2C_1)
#	define i2c_master_readReg	i2c1_master_readReg
#	define i2c_master_writeReg	i2c1_master_writeReg
#endif

//PRIVATE FUNCTIONS
/**
 *	@brief Save data within the ISL1218.
 *	Decimal format to BCD format.
 */
static void ISL1218_dec_2_bcd(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t mins, uint8_t secs, uint8_t *ISL1218)
{
	ISL1218[0] = ((year  / 10) << 4 | year  % 10);
	ISL1218[1] = ((month / 10) << 4 | month % 10);
	ISL1218[2] = ((day   / 10) << 4 | day   % 10);
	ISL1218[3] = ((hour  / 10) << 4 | hour  % 10);
	ISL1218[4] = ((mins  / 10) << 4 | mins  % 10);
	ISL1218[5] = ((secs  / 10) << 4 | secs  % 10);
}

/**
 *	@brief Get data from ISL1218.
 *  BCD format to decimal format 
 */
static void ISL1218_bcd_2_dec(uint8_t *ISL1218, uint8_t *dateTime)
{
	dateTime[0] = ((ISL1218[0] & 0xF0)>>4)*10 + (ISL1218[0] & 0x0F);
	dateTime[1] = ((ISL1218[1] & 0xF0)>>4)*10 + (ISL1218[1] & 0x0F);
	dateTime[2] = ((ISL1218[2] & 0xF0)>>4)*10 + (ISL1218[2] & 0x0F);
	dateTime[3] = ((ISL1218[3] & 0xF0)>>4)*10 + (ISL1218[3] & 0x0F);
	dateTime[4] = ((ISL1218[4] & 0xF0)>>4)*10 + (ISL1218[4] & 0x0F);
	dateTime[5] = ((ISL1218[5] & 0xF0)>>4)*10 + (ISL1218[5] & 0x0F);
}

static void ISL1218_print_dec_time(uint8_t *gRTC)
{	
	char dbg_buff[25]= "";
	sprintf(dbg_buff,"%d/%d/%d %d:%d:%d\n",gRTC[2],gRTC[1],gRTC[0],gRTC[3],gRTC[4],gRTC[5]);
	dbg_prints(dbg_buff);
	
}
//PUBLIC FUNCTIONS

/**
 *	@brief Init ISL1218
 */
void ISL1218_init(void)
{
	uint8_t data_in=0x00;
	
	//Control and Status Register
	i2c_master_readReg(ISL1218_ADDRESS, ISL1208_REG_SR, &data_in,1);
	dbg_printh(data_in);
	data_in |= ISL1208_REG_SR_ARST | ISL1208_REG_SR_WRTC; //set auto reset and write enable
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_SR, &data_in, 1);
	
	i2c_master_readReg(ISL1218_ADDRESS, ISL1208_REG_SR, &data_in,1);
	uart2_putchar('#');	//debug
	dbg_printh(data_in);//check
	
	//Interrupt Control Register
	i2c_master_readReg(ISL1218_ADDRESS, ISL1208_REG_INT, &data_in,1);
	dbg_printh(data_in);
	data_in |= ISL1208_REG_INT_ALME | ISL1208_REG_INT_IM; //enable alarm and set to single event 
	data_in &= ~(ISL1208_REG_INT_FOBATB); //disable frecuency output mode
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_INT, &data_in, 1);
	
	i2c_master_readReg(ISL1218_ADDRESS, ISL1208_REG_INT, &data_in,1);
	uart2_putchar('#');	//debug
	dbg_printh(data_in);//check
}

/**
 *	@brief Set date time to ISL1218.
 */
void ISL1218_set_time(uint8_t *bufferISL1218)
{
	dbg_prints("Set ISL1218 time");			//debug: show set ISL1218
	ISL1218_print_dec_time(bufferISL1218);	//debug to show date 
	
	uint8_t twiISL1218Set[6];
	memset(twiISL1218Set,'\0',sizeof(twiISL1218Set));
	ISL1218_dec_2_bcd(bufferISL1218[0],bufferISL1218[1],bufferISL1218[2],bufferISL1218[3],bufferISL1218[4],bufferISL1218[5],twiISL1218Set);

	
	uint8_t data_in = twiISL1218Set[0];
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_YR, &data_in, 1);
	
	data_in = twiISL1218Set[1];
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_MO, &data_in, 1);
	
	data_in = twiISL1218Set[2];
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_DT, &data_in, 1);
	
	data_in = twiISL1218Set[3];
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_HR, &data_in, 1);
	
	data_in = twiISL1218Set[4];
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_MN, &data_in, 1);
	
	data_in = twiISL1218Set[5];
	i2c_master_writeReg(ISL1218_ADDRESS,ISL1208_REG_SC, &data_in, 1);
}

/**
 *	Get date time from ISL1218.
 */
void ISL1218_get_time(uint8_t *gRTC)
{	
	dbg_prints("Get ISL1218 time"); //debug: show get ISL1218
	uint8_t twi_rtc_get[6]={0};
	
	i2c_master_readReg(ISL1218_ADDRESS,ISL1208_REG_YR, &twi_rtc_get[0],1);
	i2c_master_readReg(ISL1218_ADDRESS,ISL1208_REG_MO, &twi_rtc_get[1],1);
	i2c_master_readReg(ISL1218_ADDRESS,ISL1208_REG_DT, &twi_rtc_get[2],1);
	i2c_master_readReg(ISL1218_ADDRESS,ISL1208_REG_HR, &twi_rtc_get[3],1);
	i2c_master_readReg(ISL1218_ADDRESS,ISL1208_REG_MN, &twi_rtc_get[4],1);
	i2c_master_readReg(ISL1218_ADDRESS,ISL1208_REG_SC, &twi_rtc_get[5],1);
	
	RTC_bcd_2_dec(twi_rtc_get,gRTC);
	ISL1218_print_dec_time(gRTC);
}

/**
 *	@brief Set date time to ISL1218.
 */
void ISL1218_set_alarm(uint8_t *bufferISL1218)
{
	dbg_prints("Set ISL1218 alarm");	//debug to show function
	ISL1218_print_dec_time(bufferISL1218);	//debug to show date 
	
	uint8_t alarm[6] = {0};
	ISL1218_dec_2_bcd(bufferISL1218[0],bufferISL1218[1],bufferISL1218[2],bufferISL1218[3],bufferISL1218[4],bufferISL1218[5],alarm);
	
	uint8_t alarm_hour = (alarm[3]) | 0x80;
	//dbg_printh(alarm_hour);	//dbg
	i2c_master_writeReg(ISL1218_ADDRESS, ISL1208_REG_HRA, &alarm_hour, 1);  //set hour	alarm
	
	uint8_t alarm_min = (alarm[4]) | 0x80;
	//dbg_printh(alarm_min);	//dbg
	i2c_master_writeReg(ISL1218_ADDRESS, ISL1208_REG_MNA, &alarm_min, 1);	//set minute alarm
	
	uint8_t alarm_sec = (alarm[5])| 0x80;
	//dbg_printh(alarm_sec);	//dbg
	i2c_master_writeReg(ISL1218_ADDRESS, ISL1208_REG_SCA, &alarm_sec, 1);	//set seconds alarm
}


/**
 *	@breif Check ISL1218 parameters
 */
uint8_t ISL1218_check_configTIME(uint8_t *_t)
{

	uint8_t res = 0;
	
	if (_t[0] >= 0 && _t[0] <= 99){res += 1;}
	if (_t[1] >= 1 && _t[1] <= 12){res += 1;}
	if (_t[2] >= 1 && _t[2] <= 31){res += 1;}
	
	if (_t[3] >= 0 && _t[3] <= 23){res += 1;}
	if (_t[4] >= 0 && _t[4] <= 59){res += 1;}
	if (_t[5] >= 0 && _t[5] <= 59){res += 1;}
	
	if (res == 6)
	{
		return 1;
	}
	else
	{
		return 0;
	}
	
}

