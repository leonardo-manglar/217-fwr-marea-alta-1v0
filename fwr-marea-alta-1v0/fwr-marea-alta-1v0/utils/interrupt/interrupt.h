/*
 * interrupt.h
 *
 * Created: 18-Jul-18 11:53:48 AM
 *  Author: Diego Hinojosa
 */ 


#ifndef INTERRUPT_H_
#define INTERRUPT_H_


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

//global variable to count the number of overflows
volatile uint32_t timer0_overflow_count ;	//!< count to timer0
volatile uint32_t timer1_overflow_count ;	//!< count to timer1
volatile uint32_t timer3_overflow_count ;	//!< count to timer3
volatile uint32_t timer4_overflow_count ;	//!< count to timer4
volatile uint32_t wdt_overflow_count;		//!< Stores the wdt count 

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER0                                  */
/************************************************************************/
/* Timer 8-bit */
void timer0_init(void);
void timer0_reset(void);

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER1                                  */
/************************************************************************/
/* Timer 16-bit  */
void timer1_init(void);
void timer1_reset(void);

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER 3                                 */
/************************************************************************/
/* Timer 16-bit  */
void timer3_init(void);
void timer3_reset(void);
void timer3_reset_noStop(void);

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER 4                                 */
/************************************************************************/
/* Timer 16-bit  */
void timer4_init(void);
void timer4_reset(void);
void timer4_reset_noStop(void);

/************************************************************************/
/* External Interrupt Handler                                           */
/************************************************************************/
void Interrupt0_init(void);
void Interrupt0_enable(void);
void Interrupt0_disable(void);

void Interrupt1_init(void);
void Interrupt1_enable(void);
void Interrupt1_disable(void);

void Interrupt2_init(void);
void Interrupt2_enable(void);
void Interrupt2_disable(void);

void Interrupt_pcint0_init(void);
void Interrupt_pcint1_init(void);


/************************************************************************/
/* Watchdog Interrupt Handler                                           */
/************************************************************************/
void initWDT(void);
void stopWDT(void);

#endif /* INTERRUPT_H_ */