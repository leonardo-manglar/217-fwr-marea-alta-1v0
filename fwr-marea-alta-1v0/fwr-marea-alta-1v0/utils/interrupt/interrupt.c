/*
 * interrupt.c
 *
 * Created: 18-Jul-18 11:53:37 AM
 *  Author: Diego Hinojosa
 */ 
#include "interrupt.h"

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER0                                  */
/************************************************************************/

ISR(TIMER0_OVF_vect){
	timer0_overflow_count++;
}

void timer0_init(){
	TCCR0B |= _BV(CS02) | _BV(CS00);		// set up timer with pre scaler 1024
	TCNT0 = 0;								//initialize counter
	TIMSK0 |= _BV(TOIE0);					// enable overflow interrupt
	sei();									// enable global interrupts
	timer0_overflow_count = 0;				// initialize overflow counter variable
}

void timer0_reset(){
	TCNT0 = 0;								// reset counter
	timer0_overflow_count = 0;
	TIMSK0 &= ~_BV(TOIE0);					//turnoff overflow int
}

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER1                                  */
/************************************************************************/

ISR(TIMER1_OVF_vect){
	timer1_overflow_count++;
}

/* Timer 16-bit  */
void timer1_init(){
	TCCR1B |= _BV(CS12) | _BV(CS10);		//set up timer with pre scaler 1024
	TCNT1 = 0;								//initialize counter
	TIMSK1 |= _BV(TOIE1);					// enable overflow interrupt
	sei();									// enable global interrupts
	timer1_overflow_count = 0;				// initialize overflow counter variable
}

void timer1_reset(){
	TCNT1 = 0;								// reset counter
	timer1_overflow_count = 0;
	TIMSK1 &= ~_BV(TOIE1);					//turnoff overflow int
}

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER 3                                 */
/************************************************************************/

ISR(TIMER3_OVF_vect){
	timer3_overflow_count++;
}

void timer3_init(){
	TCCR3B |= _BV(CS32) | _BV(CS31) | _BV(CS30);		//set up timer with prescaler 1024
	TCNT3 = 0;								//initialize counter
	TIMSK3 |= _BV(TOIE3);					// enable overflow interrupt
	sei();									// enable global interrupts
	timer3_overflow_count = 0;				// initialize overflow counter variable
}

void timer3_reset(){
	TCNT3 = 0;								// reset counter
	timer3_overflow_count = 0;
	TIMSK3 &= ~_BV(TOIE3);					//turnoff overflow int
}

void timer3_reset_noStop(){
	TCNT3 = 0;								// reset counter
	timer3_overflow_count = 0;
}

/************************************************************************/
/*  Timeout Interrupt Handle    TIMER 4                                 */
/************************************************************************/

ISR(TIMER4_OVF_vect){
	timer4_overflow_count++;
}

void timer4_init(){
	TCCR4B |= _BV(CS42) | _BV(CS41) | _BV(CS40);		//set up timer with prescaler 1024
	TCNT4 = 0;								//initialize counter
	TIMSK4 |= _BV(TOIE4);					// enable overflow interrupt
	sei();									// enable global interrupts
	timer4_overflow_count = 0;				// initialize overflow counter variable
}

void timer4_reset(){
	TCNT4 = 0;								// reset counter
	timer4_overflow_count = 0;
	TIMSK4 &= ~_BV(TOIE4);					//turnoff overflow int
}

void timer4_reset_noStop(){
	TCNT4 = 0;								// reset counter
	timer4_overflow_count = 0;
}
/************************************************************************/
/* External Interrupt Handler                                           */
/************************************************************************/

/*
 * \brief Initializes the interruption 0 and sets his parameters.
 */
void Interrupt0_init(void)
{
	//EICRA |= _BV(ISC00);					// Any logical change on INT0 generates an interrupt request
	//EICRA |= _BV(ISC01);					// The  falling edge of INT0 generates an interrupt request
	EICRA |= _BV(ISC01) | _BV(ISC00);		// The rising edge of INT0 generate an interrupt request
	//EICRA &= ~_BV(ISC01) & ~_BV(ISC00);	// The low level of INT0 generates an interrupt request

	//EIMSK |= _BV(INT0);						// enable INT0 
	//sei();                                  // set (global) interrupt enable bit 
}

void Interrupt0_enable(void)
{
	EIMSK |= _BV(INT0);						// enable INT0
	sei();                                  // set (global) interrupt enable bit
}

void Interrupt0_disable(void)
{
	EIMSK &= ~(_BV(INT0));						// enable INT0
}


/*
 * \brief Initializes the interruption 1 and sets his parameters.
 */
void Interrupt1_init(void) {
	//EICRA |= _BV(ISC10);					// Any logical change on INT1 generates an interrupt request
	//EICRA |= _BV(ISC11);					// The  falling edge of INT1 generates an interrupt request
	EICRA |= _BV(ISC11) | _BV(ISC10);		// The rising edge of INT1 generate an interrupt request
	//EICRA &= ~_BV(ISC11) & ~_BV(ISC10);	// The low level of INT1 generates an interrupt request

	//EIMSK |= _BV(INT1);						// enable INT1
	//sei();                                  // set (global) interrupt enable bit
}

void Interrupt1_enable(void) 
{
	EIMSK |= _BV(INT1);						// enable INT1
	sei();                                  // set (global) interrupt enable bit
}

void Interrupt1_disable(void) 
{
	EIMSK &= ~(_BV(INT1));						// enable INT1
}

/*
 * \brief Initializes the interruption 2 and sets his parameters.
 */
void Interrupt2_init(void) {
	//EICRA |= _BV(ISC20);					// Any logical change on INT1 generates an interrupt request
	//EICRA |= _BV(ISC21);					// The  falling edge of INT1 generates an interrupt request
	EICRA |= _BV(ISC21) | _BV(ISC20);		// The rising edge of INT1 generate an interrupt request
	//EICRA &= ~_BV(ISC21) & ~_BV(ISC20);	// The low level of INT1 generates an interrupt request

	//EIMSK |= _BV(INT1);						// enable INT1
	//sei();                                  // set (global) interrupt enable bit
}

void Interrupt2_enable(void) 
{
	EIMSK |= _BV(INT2);						// enable INT1
	sei();                                  // set (global) interrupt enable bit
}

void Interrupt2_disable(void) 
{
	EIMSK &= ~(_BV(INT2));						// enable INT1
}

/*
 * \brief Init PCINT0
 */
void Interrupt_pcint0_init(void){
	PCICR |= _BV(PCIE0);
	PCMSK0 |= _BV(PCINT0);
	sei();
}

/*
 * \brief Init PCINT0
 */
void Interrupt_pcint1_init(void){
	PCICR |= _BV(PCIE1);
	PCMSK0 |= _BV(PCINT1);
	sei();
}



/************************************************************************/
/* Watchdog Interrupt Handler                                           */
/************************************************************************/
ISR(WDT_vect){
	wdt_overflow_count++;
	WDTCSR |= _BV(WDIE);	// re enabled interrupt, because WDT auto switch to reset mode
}

void initWDT(){
	wdt_overflow_count = 0;
	/* Disable and clear all Watchdog settings. Nice to get a clean slate when dealing with interrupts */
	WDTCSR = (1<<WDCE)|(1<<WDE); // Set Change Enable bit and Enable Watchdog System Reset Mode.
	WDTCSR = 0;					// Disable watchdog
	
	wdt_enable(WDTO_8S);	 // 8 seconds watchdog timer
	WDTCSR |= _BV(WDIE);	 // enable watchdog interrupt only mode 		// Pag.75 atmega328pb
	sei();
}

void stopWDT(){
	wdt_disable();
}
