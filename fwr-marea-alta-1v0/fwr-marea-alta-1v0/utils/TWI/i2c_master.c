/*
 * i2c_master.c
 *
 * Created: 18-Jul-18 4:05:21 PM
 *  Author: Diego Hinojosa
 */ 

#include <avr/io.h>
#include <util/twi.h>

#include "i2c_master.h"

 
 #define TWI_FREQ 400000
#define F_SCL 400000UL // SCL frequency
#define Prescaler 1
#define TWBR_val ((((F_CPU / F_SCL) / Prescaler) - 16 ) / 2)
//#define GET_TWBR ((F_CPU/TWI_FREQ)-16)/2

/************************************************************************/
/* I2C 0                                                                */
/************************************************************************/

void i2c0_master_init(void)
{
	TWBR0 = TWBR_val;
	TWCR0 = _BV(TWEN);	//enable TWI
}

uint8_t i2c0_master_start(uint8_t address)
{
	TWCR0 = 0;	// reset TWI control reg
	TWCR0 = _BV(TWINT)|_BV(TWSTA)|_BV(TWEN);	// start condition bus
	
	while (!(TWCR0 & _BV(TWINT)));			// Wait for end of transmission
	
	if ((TWSR0 & 0xF8) != TW_START) return 1;
	
	TWDR0 = address;
	TWCR0 = _BV(TWINT)|_BV(TWEN);
	
	while (!(TWCR0 & _BV(TWINT)));

	uint8_t twst = TWSR0 & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ){  //SLA+W transmitted, ACK received[0x18] && SLA+R transmitted, ACK received [0x40]
		//dbg_prints("i2c_error_ack");
		return 1;
	}
	return 0;
}

uint8_t i2c0_master_write(uint8_t data)
{
	// load data into data register
	TWDR0 = data;
	// start transmission of data
	TWCR0 = (1<<TWINT) | (1<<TWEN);
	
	// wait for end of transmission
	while( !(TWCR0 & (1<<TWINT)) );

	if( (TWSR0 & 0xF8) != TW_MT_DATA_ACK ){ return 1; }

	return 0;
}

uint8_t i2c0_master_read_ack(void)
{

	// start TWI module and acknowledge data after reception
	TWCR0 = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	// wait for end of transmission
	while( !(TWCR0 & (1<<TWINT)) );
	// return received data from TWDR
	return TWDR0;
}

uint8_t i2c0_master_read_nack(void)
{

	// start receiving without acknowledging reception
	TWCR0 = (1<<TWINT) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR0 & (1<<TWINT)) );
	// return received data from TWDR
	return TWDR0;
}


uint8_t i2c0_master_writeReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length)
{
	if (i2c0_master_start(devaddr<<1)) return 1;

	i2c0_master_write(regaddr);

	for (uint16_t i = 0; i < length; i++)
	{
		if (i2c0_master_write(data[i])) return 1;
	}

	i2c0_master_stop();

	return 0;
}

uint8_t i2c0_master_readReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length)
{
	_delay_ms(10);
	if (i2c0_master_start(devaddr<<1)) return 1;
	_delay_ms(10);
	i2c0_master_write(regaddr);

	if (i2c0_master_start((devaddr<<1) | 0x01)) return 1;

	for (uint16_t i = 0; i < (length-1); i++)
	{
		data[i] = i2c0_master_read_ack();
	}
	data[(length-1)] = i2c0_master_read_nack();

	i2c0_master_stop();

	return 0;
}

void i2c0_master_stop(void)
{
	// transmit STOP condition
	TWCR0 = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
}

/************************************************************************/
/* I2C 1                                                                */
/************************************************************************/

void i2c1_master_init(void)
{
	TWBR1 = TWBR_val;
	TWCR1 = _BV(TWEN);	//enable TWI
}

uint8_t i2c1_master_start(uint8_t address)
{

	TWCR1 = 0;	// reset TWI control reg
	TWCR1 = _BV(TWINT)|_BV(TWSTA)|_BV(TWEN);	// start condition bus
	
	while (!(TWCR1 & _BV(TWINT)));			// Wait for end of transmission
	
	if ((TWSR1 & 0xF8) != TW_START) return 1;
	
	TWDR1 = address;
	TWCR1 = _BV(TWINT)|_BV(TWEN);
	
	while (!(TWCR1 & _BV(TWINT)));
	
	//dbg_printSh("twst:",TWSR1 & 0xF8);//dbg
	uint8_t twst = TWSR1 & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ){  //SLA+W transmitted, ACK received[0x18] && SLA+R transmitted, ACK received [0x40]
		//dbg_prints("i2c_error_ack");
		return 1;
	}
	return 0;
	
}

uint8_t i2c1_master_write(uint8_t data)
{
	// load data into data register
	TWDR1 = data;
	// start transmission of data
	TWCR1 = (1<<TWINT) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR1 & (1<<TWINT)) );

	if( (TWSR1 & 0xF8) != TW_MT_DATA_ACK ){ return 1; }

	return 0;
}

uint8_t i2c1_master_read_ack(void)
{

	// start TWI module and acknowledge data after reception
	TWCR1 = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	// wait for end of transmission
	while( !(TWCR1 & (1<<TWINT)) );
	// return received data from TWDR
	return TWDR1;
}

uint8_t i2c1_master_read_nack(void)
{

	// start receiving without acknowledging reception
	TWCR1 = (1<<TWINT) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR1 & (1<<TWINT)) );
	// return received data from TWDR
	return TWDR1;
}



uint8_t i2c1_master_writeReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length)
{
	if (i2c1_master_start(devaddr<<1)) return 1;

	i2c1_master_write(regaddr);

	for (uint16_t i = 0; i < length; i++)
	{
		if (i2c1_master_write(data[i])) return 1;
	}

	i2c1_master_stop();

	return 0;
}

uint8_t i2c1_master_readReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length)
{
	_delay_ms(10);
	if (i2c1_master_start(devaddr<<1)) return 1;
	_delay_ms(10);
	i2c1_master_write(regaddr);

	if (i2c1_master_start((devaddr<<1) | 0x01)) return 1;

	for (uint16_t i = 0; i < (length-1); i++)
	{
		data[i] = i2c1_master_read_ack();
	}
	data[(length-1)] = i2c1_master_read_nack();

	i2c1_master_stop();

	return 0;
}

void i2c1_master_stop(void)
{
	// transmit STOP condition
	TWCR1 = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
}

/************************************************************************/
/* COMMON                                                               */
/************************************************************************/

void i2c_master_detect(uint8_t i2c_num)
{
	uint8_t devaddr;
	uint8_t res;
	for(devaddr=0x00; devaddr<=0xfe; devaddr++)
	{
		if (i2c_num==0)
		{
			res = i2c0_master_start(devaddr);
		}else
		{
			res = i2c1_master_start(devaddr);
		}
		if(!res)
		{
			dbg_printh(devaddr>>1);
			devaddr++;
		}
		_delay_ms(10);
	}
}

void i2c_dummy_delay(uint32_t delay_ms)
{
	
}