/*
 * i2c_master.h
 *
 * Created: 18-Jul-18 4:05:31 PM
 *  Author: Diego Hinojosa
 */ 


#ifndef I2C_MASTER_H_
#define I2C_MASTER_H_

#ifndef F_CPU
#define F_CPU 16000000UL //!< Clock Frequency
#endif
#include <util/delay.h>
#include "serial_debug.h"

#define I2C_READ 0x01
#define I2C_WRITE 0x00

/************************************************************************/
/* I2C 0                                                                */
/************************************************************************/
void i2c0_master_init(void);
uint8_t i2c0_master_start(uint8_t address);
uint8_t i2c0_master_write(uint8_t data);
uint8_t i2c0_master_read_ack(void);
uint8_t i2c0_master_read_nack(void);
uint8_t i2c0_master_transmit(uint8_t address, uint8_t* data, uint16_t length);
uint8_t i2c0_master_receive(uint8_t address, uint8_t* data, uint16_t length);
uint8_t i2c0_master_writeReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length);
uint8_t i2c0_master_readReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length);
void i2c0_master_stop(void);

/************************************************************************/
/* I2C 1                                                                */
/************************************************************************/
void i2c1_master_init(void);
uint8_t i2c1_master_start(uint8_t address);
uint8_t i2c1_master_write(uint8_t data);
uint8_t i2c1_master_read_ack(void);
uint8_t i2c1_master_read_nack(void);
uint8_t i2c1_master_writeReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length);
uint8_t i2c1_master_readReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length);
void i2c1_master_stop(void);

/************************************************************************/
/* COMMON                                                               */
/************************************************************************/
void i2c_master_detect(uint8_t i2c_num);
void i2c_dummy_delay(uint32_t delay_ms);

void i2c_dummy_delay(uint32_t delay_ms);


#endif /* I2C_MASTER_H_ */