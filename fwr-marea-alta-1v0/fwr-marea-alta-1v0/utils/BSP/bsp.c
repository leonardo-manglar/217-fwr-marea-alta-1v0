/*
 * bsp.c
 *
 * Created: 25-Jul-18 10:22:44 AM
 *  Author: Diego Hinojosa
 */ 

#include "bsp.h"

/************************************************************************/
/*	LEDS                                                                */
/************************************************************************/
/**
 * Function LedOn
 * \brief On the actuator, id_act can be P0-P2
 */
void led_on(uint8_t id_act){
	PORTB |= _BV(id_act);
}

/**
 * Function LedOff
 * \brief On the actuator, id_act can be  P0-P2
 */
void led_off(uint8_t id_act){
	PORTB &= ~(_BV(id_act));
}

/**
 * Function LedToggle
 * \brief On the actuator, id_act can be  P0-P2
 */
void led_toggle(uint8_t id_act){
	PORTB ^= _BV(id_act);
}


/************************************************************************/
/*	ACTUATORS                                                           */
/************************************************************************/
/**
 * Function actuatorOn
 * \brief On the actuator, id_act can be P0-P2
 */
void actuator_on(uint8_t id_act){
	PORTC |= _BV(id_act);
}

/**
 * Function actuatorOff
 * \brief On the actuator, id_act can be  P0-P2
 */
void actuator_off(uint8_t id_act){
	PORTC &= ~(_BV(id_act));
}


/************************************************************************/
/*                                                                      */
/************************************************************************/

void reset_micro(void)
{
	// go for immediate reset
	WDTCSR = _BV(WDCE) | _BV(WDE);	 // Enable the WD Change Bit - configure mode
	WDTCSR = _BV(WDE)  | _BV(WDP0);  // set reset flag (WDE) and 16ms (WDP0)
	_delay_ms(20);
}

void wdt_init(void){
	MCUSR = 0;	//clear reset flags
	wdt_disable();
	return;
}