/*
 * bsp.h
 *
 * Created: 25-Jul-18 10:22:35 AM
 *  Author: Diego Hinojosa
 */ 


#ifndef BSP_H_
#define BSP_H_

#include <avr/io.h>
#ifndef F_CPU
#define F_CPU 160000000UL
#endif
#include <util/delay.h>
#include <avr/wdt.h>

/**
 * Function LedOn
 * \brief On the actuator, id_act can be P0-P2
 */
void led_on(uint8_t id_act);

/**
 * Function LedOff
 * \brief On the actuator, id_act can be  P0-P2
 */
void led_off(uint8_t id_act);

/**
 * Function LedToggle
 * \brief On the actuator, id_act can be  P0-P2
 */
void led_toggle(uint8_t id_act);


/**
 * Function actuatorOn
 * \brief On the actuator, id_act can be P0-P2
 */
void actuator_on(uint8_t id_act);

/**
 * Function actuatorOff
 * \brief On the actuator, id_act can be  P0-P2
 */
void actuator_off(uint8_t id_act);


void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3"))); //reset
void reset_micro(void);


#endif /* BSP_H_ */